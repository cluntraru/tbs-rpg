#include <world.h>

#include <iostream>

#include <asset/assimpassetloader.h>
#include <camera.h>
#include <controls/defaultcontrolscomponent.h>
#include <controls/gamepadcontrolscomponent.h>
#include <data/jsondataprovider.h>
#include <data/manualdataprovider.h>
#include <drawable.h>
#include <entity.h>
#include <gameplay/piece.h>
#include <games/chess/chessgameplaycomponent.h>
#include <games/go/gogameplaycomponent.h>
#include <render/defaultrenderingcomponent.h>
#include <window/displaycontext.h>

World::World(const std::string &gameName, ControlsType controlsType, RenderingType renderingType,
             AssetLoaderType assetLoaderType, GameplayType gameplayType,
             DataProviderType dataProviderType) {
    m_ShouldEnd = false;

    m_GameName = gameName;

    switch (controlsType) {
     case GAMEPAD_CONTROLS:
        m_ControlsComponent = new GamepadControlsComponent(this);
        break;
     case DEFAULT_CONTROLS:
     default:
        m_ControlsComponent = new DefaultControlsComponent(this);
    }

    switch (renderingType) {
     case DEFAULT_RENDERING:
     default:
        m_RenderingComponent = new DefaultRenderingComponent(this);
    }

    switch (assetLoaderType) {
     case ASSIMP_LOADER:
     default:
        m_AssetLoader = new AssimpAssetLoader(m_RenderingComponent);
    }

    switch (gameplayType) {
     case GAMEPLAY_GO:
        m_GameplayComponent = new GoGameplayComponent(this);
        break;
     case GAMEPLAY_CHESS:
     default:
        m_GameplayComponent = new ChessGameplayComponent(this);
    }

    switch (dataProviderType) {
     case MANUAL_DATAPROVIDER:
        m_DataProvider = new ManualDataProvider();
        break;
     case JSON_DATAPROVIDER:
     default:
        m_DataProvider = new JsonDataProvider(gameName);
    }
}

World::~World() {
    delete m_RenderingComponent;
    delete m_ControlsComponent;
}

int World::Init() {
    m_Camera = new Camera();
    if (m_Camera == nullptr) {
        return -1;
    }

    int renderingError = m_RenderingComponent->Init();
    if (renderingError) {
        printf("Rendering init error, aborting\n");
        Destroy();
        return renderingError;
    }

    int controlsError = m_ControlsComponent->Init();
    if (controlsError) {
        printf("Controls init error, aborting\n");
        Destroy();
        return controlsError;
    }

    int gameplayError = m_GameplayComponent->Init();
    if (gameplayError) {
        printf("Gameplay init error, aborting\n");
        Destroy();
        return gameplayError;
    }

    return 0;
}

void World::Start() {
    m_PreviousFrameTime = DisplayContext::GetInstance()->GetCurrentTime();

    m_RenderingComponent->Start();
    m_ControlsComponent->Start();
    m_GameplayComponent->Start();
}

void World::Update() {
    float currentTime = DisplayContext::GetInstance()->GetCurrentTime();
    m_DeltaTime = currentTime - m_PreviousFrameTime;
    m_PreviousFrameTime = currentTime;

    m_RenderingComponent->Update(m_DeltaTime);
    m_ControlsComponent->Update(m_DeltaTime);
    m_GameplayComponent->Update(m_DeltaTime);
}

void World::Stop() {
    m_GameplayComponent->Stop();
    m_ControlsComponent->Stop();
    m_RenderingComponent->Stop();
}

void World::Destroy() {
    if (m_GameplayComponent != nullptr) {
        m_GameplayComponent->Destroy();
    }

    if (m_ControlsComponent != nullptr) {
        m_ControlsComponent->Destroy();
    }

    if (m_RenderingComponent != nullptr) {
        m_RenderingComponent->Destroy();
    }

    delete m_Camera;
    m_Camera = nullptr;
}

void World::AddRootEntity(Entity *entity) {
    m_RootEntities.push_back(entity);
}

void World::RemoveRootEntity(Entity *entity) {
    m_RootEntities.remove(entity);
}

std::vector<Drawable> World::FlattenEntities() {
    std::vector<Drawable> flattenedEntities;
    for (Entity *entity : m_RootEntities) {
        FlattenChildren(flattenedEntities, Transform::s_Identity.CalcMatrix(), entity);
    }

    return flattenedEntities;
}

Drawable World::FlattenEntity(Entity *entity) {
    std::vector<glm::mat4> transformChain;
    transformChain.push_back(entity->m_Transform.CalcMatrix());
    while (Entity *parent = entity->GetParent()) {
        transformChain.push_back(parent->m_Transform.CalcMatrix());
        entity = parent;
    }

    glm::mat4 chainTransform = Transform::s_Identity.CalcMatrix();
    for (int i = transformChain.size() - 1; i >= 0; --i) {
        chainTransform = chainTransform * transformChain[i];
    }

    return Drawable(chainTransform, entity->GetModel());
}

void World::FlattenChildren(std::vector<Drawable> &flatEntities,
                            const glm::mat4 &parentChainTransform,
                            Entity *entity) const {
    const std::list<Entity *> &children = entity->GetChildren();
    const glm::mat4 &chainTransform = parentChainTransform * entity->m_Transform.CalcMatrix();

    Piece *piece = dynamic_cast<Piece *>(entity);
    const bool isTransparent = piece && piece->GetIsChosen();

    Model *const &model = entity->GetModel();
    if (model != nullptr) {
        flatEntities.emplace_back(Drawable(chainTransform, model, isTransparent));
    }

    for (Entity *child : children) {
        FlattenChildren(flatEntities, chainTransform, child);
    }
}

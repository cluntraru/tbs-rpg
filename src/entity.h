#ifndef SRC_ENTITY_H__
#define SRC_ENTITY_H__

#include <transform.h>

#include <list>
#include <string>

class Model;
class World;

class Entity {
 public:
    explicit Entity(World *const &world);
    Entity(World *const &world, const Transform &transform);
    virtual ~Entity();

    inline World *GetWorld() const { return m_World; }
    inline Model *GetModel() const { return m_Model; }

    void LoadModel(const std::string &modelPath);

    inline Entity *GetParent() const { return m_Parent; }
    inline const std::list<Entity *> &GetChildren() const { return m_Children; }

    virtual void AddChild(Entity *entity);
    virtual void RemoveChild(Entity *entity);

    Transform m_Transform;

    const Entity &operator =(const Entity &) = delete;

 private:
    Model *m_Model = nullptr;
    World *m_World;

    Entity *m_Parent = nullptr;
    // This needs to bee a DAG
    std::list<Entity *> m_Children;
};

#endif  // SRC_ENTITY_H__

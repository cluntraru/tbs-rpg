#include <camera.h>

#include <cstdio>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <window/displaycontext.h>
#include <window/glfwwindowdata.h>

const float &Camera::s_PitchLimit = 80.f;
const float &Camera::s_FrustumNear = 0.1f;
const float &Camera::s_FrustumFar = 1000.f;

Camera::Camera() {
    InitPerspectiveMatrix();
}

Camera::Camera(const glm::vec4 &position) {
    InitPerspectiveMatrix();
    m_Transform.SetTranslation(position);
}

Camera::Camera(const glm::vec4 &position, const glm::vec4 &direction) {
    InitPerspectiveMatrix();
    m_Transform.SetTransform(position, direction);
}

Camera::~Camera() {
}

glm::mat4 Camera::CalcViewMatrix() const {
    const glm::vec3 &position = glm::vec3(m_Transform.GetTranslation());
    const glm::vec3 &direction = glm::vec3(m_Transform.GetDirection());
    const glm::vec3 &frontPosition = position + direction;
    const glm::vec3 &up = m_Transform.GetUp();
    return glm::lookAt(position, frontPosition, up);
}

void Camera::Translate(TranslateType translateType, const float &meters) {
    glm::vec3 up(m_Transform.GetUp());
    glm::vec3 right(m_Transform.GetRight());
    glm::vec3 front(m_Transform.GetDirection());
    switch (translateType) {
     case UP:
        m_Transform.Translate(meters * up);
        break;
     case RIGHT:
        m_Transform.Translate(meters * right);
        break;
     case FORWARD:
        m_Transform.Translate(meters * front);
        break;
     default:
        printf("Unsupported camera translation, skipping\n");
    }
}

void Camera::Rotate(RotateType rotateType, const float &degrees) {
    const float &radians = glm::radians(degrees);
    switch (rotateType) {
     case PITCH:
        m_Transform.ApplyPitch(CalcPitch(radians));
        break;
     case YAW:
        m_Transform.ApplyYaw(radians);
        break;
     case ROLL:
        m_Transform.ApplyRoll(radians);
        break;
     default:
        printf("Unsupported camera rotation, skipping\n");
    }
}

float Camera::GetAspectRatio() const {
    const float &defaultAspectRatio = 16.f / 9.f;
    DisplayContext *displayContext = DisplayContext::GetInstance();
    if (displayContext == nullptr) {
        printf("No display context available, returning 16:9\n");
        return defaultAspectRatio;
    }

    GLFWWindowData *windowData = displayContext->GetWindowData();
    if (windowData == nullptr) {
        printf("No window data available, returning 16:9\n");
        return defaultAspectRatio;
    }

    const float &width = static_cast<float>(windowData->GetWidth());
    const float &height = static_cast<float>(windowData->GetHeight());
    return width / height;
}

void Camera::InitPerspectiveMatrix() {
    const float &aspectRatio = GetAspectRatio();
    m_Projection = glm::perspective(glm::radians(45.f), aspectRatio, s_FrustumNear, s_FrustumFar);
}

float Camera::CalcPitch(float candidatePitch) const {
    float pitch = m_Transform.GetOXAngle();
    float pitchLimit = glm::radians(s_PitchLimit);

    if (MathHelper::lt(pitch + candidatePitch, -pitchLimit)) {
        candidatePitch = -pitchLimit - pitch;
    } else if (MathHelper::gt(pitch + candidatePitch, pitchLimit)) {
        candidatePitch = pitchLimit - pitch;
    }

    return candidatePitch;
}

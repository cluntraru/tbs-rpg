#ifndef SRC_RENDER_SHADERPROGRAM_H_
#define SRC_RENDER_SHADERPROGRAM_H_

#include <glad/glad.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <glm/glm.hpp>

class ShaderProgram {
 public:
    static void SetUnifBool(const unsigned int &location, bool value);
    static void SetUnifInt(const unsigned int &location, int value);
    static void SetUnifFloat(const unsigned int &location, float value);

    static void SetUnifVec2(const unsigned int &location, const glm::vec2 &value);
    static void SetUnifVec3(const unsigned int &location, const glm::vec3 &value);
    static void SetUnifVec4(const unsigned int &location, const glm::vec4 &value);

    static void SetUnifMat2(const unsigned int &location, const glm::mat2 &mat);
    static void SetUnifMat3(const unsigned int &location, const glm::mat3 &mat);
    static void SetUnifMat4(const unsigned int &location, const glm::mat4 &mat);

    ShaderProgram();

    int Init(const std::string &vertexShaderPath, const std::string &fragmentShaderPath);
    void Use() const;

    unsigned int GetID() const { return m_ProgramID; }

 private:
    GLchar *LoadShader(const std::string &shaderPath);
    int CompileVertexShader(const std::string &vertexShaderPath);
    int CompileFragmentShader(const std::string &fragmentShaderPath);
    int LinkProgram();

    void PrintShaderCompileLog(GLuint shader) const;
    void PrintProgramLinkLog() const;

    GLuint m_ProgramID;
};

#endif  // SRC_RENDER_SHADERPROGRAM_H_


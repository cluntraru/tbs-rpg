#include <glm/glm.hpp>

#include <render/vertex.h>

Vertex::Vertex(const glm::vec4 &position, const glm::vec4 &normal, const glm::vec2 &textureCoord) {
    m_Position = position;
    m_Normal = normal;
    m_TextureCoord = textureCoord;
}

Vertex::Vertex(const glm::vec3 &position, const glm::vec3 &normal, const glm::vec2 &textureCoord) {
    m_Position = glm::vec4(position, 1.f);
    m_Normal = glm::vec4(normal, 0.f);
    m_TextureCoord = textureCoord;
}

Vertex::~Vertex() {
}


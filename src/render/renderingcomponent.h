#ifndef SRC_RENDER_RENDERINGCOMPONENT_H__
#define SRC_RENDER_RENDERINGCOMPONENT_H__

#include <map>

#include <component.h>

class AssetLoader;
class Material;
class Mesh;
class Model;
class Texture;

class RenderingComponent: public Component {
 public:
    explicit RenderingComponent(World *world);
    virtual ~RenderingComponent();

    virtual int DrawModel(Model *const &model) = 0;

    virtual Model *RegisterModel();
    virtual Texture *RegisterTexture();
    virtual Material *RegisterMaterial();

    virtual std::map<unsigned int, Model*>::iterator DeregisterModel(unsigned int id);
    virtual std::map<unsigned int, Texture*>::iterator DeregisterTexture(unsigned int id);
    virtual std::map<unsigned int, Material*>::iterator DeregisterMaterial(unsigned int id);

    virtual void DeregisterModels();
    virtual void DeregisterTextures();
    virtual void DeregisterMaterials();

    virtual Model *GetModel(unsigned int id) const;
    virtual Material *GetMaterial(unsigned int id) const;
    virtual Texture *GetTexture(unsigned int id) const;

     void Destroy() override;

 private:
    std::map<unsigned int, Model *> m_Models;
    std::map<unsigned int, Texture *> m_Textures;
    std::map<unsigned int, Material *> m_Materials;

    unsigned int m_LastRegisteredModelID = 0;
    unsigned int m_LastRegisteredTextureID = 0;
    unsigned int m_LastRegisteredMaterialID = 0;
};

#endif  // SRC_RENDER_RENDERINGCOMPONENT_H__


#include <climits>

#include <algorithm>
#include <map>

#include <asset/assimpassetloader.h>
#include <asset/model.h>
#include <asset/texture.h>
#include <asset/material.h>
#include <render/renderingcomponent.h>
#include <iostream>

RenderingComponent::RenderingComponent(World *world): Component(world) {
}

RenderingComponent::~RenderingComponent() {
}

void RenderingComponent::Destroy() {
    DeregisterTextures();
    std::cout << "Deregistered textures\n";
    DeregisterMaterials();
    std::cout << "Deregistered materials\n";
    DeregisterModels();
    std::cout << "Deregistered models\n";
}

Model *RenderingComponent::RegisterModel() {
    if (m_LastRegisteredModelID == UINT_MAX) {
        printf("No more indices available for new models\n");
        return nullptr;
    }

    Model *model = new Model(++m_LastRegisteredModelID);

    unsigned int modelID = model->GetID();
    m_Models[modelID] = model;

    return model;
}

std::map<unsigned int, Model*>::iterator RenderingComponent::DeregisterModel(unsigned int id) {
    auto it = m_Models.find(id);
    if (it == m_Models.end()) {
        return ++it;
    }

    Model *model = it->second;
    it = m_Models.erase(it);
    delete model;
    return it;
}

void RenderingComponent::DeregisterModels() {
    for (auto it = m_Models.begin(); it != m_Models.end();) {
        Model *model = it->second;
        it = DeregisterModel(model->GetID());
    }
}

Texture *RenderingComponent::RegisterTexture() {
    if (m_LastRegisteredTextureID == UINT_MAX) {
        printf("No more indices available for new textures\n");
        return nullptr;
    }

    Texture *texture = new Texture(++m_LastRegisteredTextureID);

    unsigned int textureID = texture->GetID();
    m_Textures[textureID] = texture;

    return texture;
}

std::map<unsigned int, Texture*>::iterator RenderingComponent::DeregisterTexture(unsigned int id) {
    auto it = m_Textures.find(id);
    if (it == m_Textures.end()) {
        return ++it;
    }

    Texture *texture = it->second;
    it = m_Textures.erase(it);
    delete texture;
    return it;
}

void RenderingComponent::DeregisterTextures() {
    for (auto it = m_Textures.begin(); it != m_Textures.end();) {
        Texture *texture = it->second;
        it = DeregisterTexture(texture->GetID());
    }
}

Material *RenderingComponent::RegisterMaterial() {
    if (m_LastRegisteredMaterialID == UINT_MAX) {
        printf("No more indices available for new materials\n");
        return nullptr;
    }

    Material *material = new Material(++m_LastRegisteredMaterialID);

    unsigned int materialID = material->GetID();
    m_Materials[materialID] = material;

    return material;
}

std::map<unsigned int, Material*>::iterator RenderingComponent::DeregisterMaterial(unsigned int id) {
    auto it = m_Materials.find(id);
    if (it == m_Materials.end()) {
        return ++it;
    }

    Material *material = it->second;
    it = m_Materials.erase(it);
    delete material;
    return it;
}

void RenderingComponent::DeregisterMaterials() {
    for (auto it = m_Materials.begin(); it != m_Materials.end();) {
        Material *material = it->second;
        it = DeregisterMaterial(material->GetID());
    }
}

Model *RenderingComponent::GetModel(unsigned int id) const {
    if (m_Models.count(id) == 0) {
        printf("No model with ID %d was registered\n", id);
        return nullptr;
    }

    return m_Models.find(id)->second;
}

Texture *RenderingComponent::GetTexture(unsigned int id) const {
    if (m_Textures.count(id) == 0) {
        printf("No texture with ID %d was registered\n");
        return nullptr;
    }

    return m_Textures.find(id)->second;
}

Material *RenderingComponent::GetMaterial(unsigned int id) const {
    if (m_Materials.count(id) == 0) {
        printf("No material with ID %d was registered\n", id);
        return nullptr;
    }

    return m_Materials.find(id)->second;
}

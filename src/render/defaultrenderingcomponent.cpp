#include <render/defaultrenderingcomponent.h>

#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include <vector>

#include <asset/assetloader.h>
#include <asset/material.h>
#include <asset/mesh.h>
#include <asset/model.h>
#include <asset/texture.h>
#include <camera.h>
#include <data/dataprovider.h>
#include <drawable.h>
#include <gameplay/slot.h>
#include <helper/mathhelper.h>
#include <render/shaderprogram.h>
#include <render/vertex.h>
#include <window/displaycontext.h>
#include <window/glfwwindowdata.h>
#include <world.h>

const float DefaultRenderingComponent::s_TransparentAlpha = 0.5f;
const glm::vec3 &DefaultRenderingComponent::s_InitialAmbientAlbedo = glm::vec3(0.5f);
const glm::vec3 &DefaultRenderingComponent::s_EndGameAmbientAlbedo = glm::vec3(0.05f, 0.05f, 0.15f);

DefaultRenderingComponent::DefaultRenderingComponent(World *world): RenderingComponent(world) {
}

DefaultRenderingComponent::~DefaultRenderingComponent() {
}

int DefaultRenderingComponent::Init() {
    m_ShaderProgram = new ShaderProgram();
    int programInitError = m_ShaderProgram->Init("src/shaders/defaultvertex.glsl",
                                                 "src/shaders/defaultfragment.glsl");

    if (programInitError) {
        printf("Program init failed\n");
        Destroy();
        return -1;
    }

    m_OverlayShaderProgram = new ShaderProgram();
    int overlayProgramInitError = m_OverlayShaderProgram->Init("src/shaders/overlayvertex.glsl",
                                                               "src/shaders/overlayfragment.glsl");

    if (overlayProgramInitError) {
        printf("Overlay program init failed\n");
        Destroy();
        return -1;
    }

    return 0;
}

void DefaultRenderingComponent::Start() {
    glClearColor(0.70f, 0.95f, 1.00f, 1.0f);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    // Blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    World *world = GetWorld();
    if (world == nullptr) {
        std::cout << "No world available, exiting\n";
        return;
    }

    AssetLoader *assetLoader = world->GetAssetLoader();
    DataProvider *dataProvider = world->GetDataProvider();
    m_OverlayModel = assetLoader->LoadModel(dataProvider->GetSlotOverlayModelPath());
}

void DefaultRenderingComponent::Update(float deltaTime) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    World *world = GetWorld();
    if (world == nullptr) {
        return;
    }

    GLFWWindowData *windowData = DisplayContext::GetInstance()->GetWindowData();
    if (windowData == nullptr) {
        std::cout << "No world available, exiting\n";
        return;
    }

    GameplayComponent *gameplayComponent = world->GetGameplayComponent();
    if (gameplayComponent->IsGameOver()) {
        m_EndGameDeltaTime += deltaTime;
    } else {
        m_EndGameDeltaTime = 0.f;
    }

    DrawEntities(world);
    DrawSlotOverlays(world);

    glfwSwapBuffers(windowData->GetWindow());
    glfwPollEvents();
}

void DefaultRenderingComponent::DrawEntities(World *const &world) {
    m_ShaderProgram->Use();
    glm::mat4 projectionMatrix = world->GetCamera()->GetProjectionMatrix();
    glm::mat4 viewMatrix = world->GetCamera()->CalcViewMatrix();

    ShaderProgram::SetUnifMat4(1, viewMatrix);
    ShaderProgram::SetUnifMat4(2, projectionMatrix);

    if (MathHelper::gt(m_EndGameDeltaTime, 0.f) && MathHelper::lt(m_EndGameDeltaTime, 2.f)) {
        float secondsToLightChange = 2.f;
        m_AmbientAlbedo = MathHelper::Lerp(s_InitialAmbientAlbedo, s_EndGameAmbientAlbedo,
                m_EndGameDeltaTime / secondsToLightChange);
    }

    ShaderProgram::SetUnifVec3(4, m_AmbientAlbedo);

    const std::vector<Drawable> &drawables = world->FlattenEntities();
    for (Drawable drawable : drawables) {
        if (drawable.model == nullptr || drawable.isTransparent) {
            continue;
        }

        ShaderProgram::SetUnifMat4(0, drawable.transform);

        const float alpha = drawable.isTransparent ? s_TransparentAlpha : 1.f;
        ShaderProgram::SetUnifFloat(8, alpha);

        DrawModel(drawable.model);
    }

    for (Drawable drawable : drawables) {
        if (drawable.model == nullptr || !drawable.isTransparent) {
            continue;
        }

        ShaderProgram::SetUnifMat4(0, drawable.transform);

        const float alpha = drawable.isTransparent ? s_TransparentAlpha : 1.f;
        ShaderProgram::SetUnifFloat(8, alpha);

        DrawModel(drawable.model);
    }
}

void DefaultRenderingComponent::DrawSlotOverlays(World *const &world) {
    m_OverlayShaderProgram->Use();
    glm::mat4 projectionMatrix = world->GetCamera()->GetProjectionMatrix();
    glm::mat4 viewMatrix = world->GetCamera()->CalcViewMatrix();

    ShaderProgram::SetUnifMat4(1, viewMatrix);
    ShaderProgram::SetUnifMat4(2, projectionMatrix);

    GameplayComponent *gpComponent = world->GetGameplayComponent();
    std::vector<Slot *> slots = gpComponent->GetBoard()->GetSlots();
    for (Slot *slot : slots) {
        Slot::SelectedType selectedType = slot->GetSelectedType();
        ShaderProgram::SetUnifInt(3, selectedType);

        if (selectedType != Slot::DESELECTED) {
            Drawable drawable = GenerateOverlayDrawable(world, slot);
            ShaderProgram::SetUnifMat4(0, drawable.transform);
            DrawModel(m_OverlayModel);
        }
    }
}

Drawable DefaultRenderingComponent::GenerateOverlayDrawable(World *const world,
                                                            const Slot *const &slot) {
    Entity slotCpy = *slot;
    DataProvider *dataProvider = world->GetDataProvider();

    slotCpy.m_Transform.Translate(dataProvider->GetSlotOverlayTranslate());
    slotCpy.m_Transform.SetScale(dataProvider->GetSlotOverlayScale());

    Drawable drawable = world->FlattenEntity(&slotCpy);
    return drawable;
}

void DefaultRenderingComponent::Stop() {
}

void DefaultRenderingComponent::Destroy() {
    RenderingComponent::Destroy();
    delete m_ShaderProgram;
}

int DefaultRenderingComponent::DrawModel(Model *const &model) {
    if (model == nullptr) {
        printf("Model does not exist\n");
        return -1;
    }

    const std::vector<Mesh *> meshes = model->GetMeshes();
    for (Mesh *mesh : meshes) {
        DrawMesh(model, mesh);
    }

    return 0;
}

int DefaultRenderingComponent::DrawMesh(Model *const &model, Mesh *const &mesh) {
    GLuint VAO;
    int setupMeshError = SetupMesh(VAO, model, mesh);
    if (setupMeshError) {
        return setupMeshError;
    }

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, mesh->GetDrawIndices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    return 0;
}

int DefaultRenderingComponent::SetupMesh(GLuint &VAO, Model *const &model, Mesh *const &mesh) {
    if (mesh == nullptr) {
        printf("Mesh does not exist, aborting setup\n");
        return -1;
    }

    int setupMeshTexturesError = SetupMeshTextures(mesh);
    if (setupMeshTexturesError) {
        printf("Texture setup for mesh with ID %d failed, continuing without textures\n",
               mesh->GetID());
    }

    unsigned int modelID = model->GetID();
    unsigned int meshID = mesh->GetID();
    if (m_MeshVAO.count({modelID, meshID}) != 0) {
        VAO = m_MeshVAO[{modelID, meshID}];
        return 0;
    }

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    const std::vector<Vertex> &vertices = mesh->GetVertices();
    const GLvoid *vertexData = &vertices[0];
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertexData, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<void *>(offsetof(Vertex, m_Position)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<void *>(offsetof(Vertex, m_TextureCoord)));
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<void *>(offsetof(Vertex, m_Normal)));
    glEnableVertexAttribArray(2);


    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    const std::vector<unsigned int> &drawIndices = mesh->GetDrawIndices();
    const GLuint *indexData = &drawIndices[0];
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, drawIndices.size() * sizeof(unsigned int), indexData,
                 GL_STATIC_DRAW);

    glBindVertexArray(0);

    // TODO(cluntraru): encapsulate
    m_MeshVAO[{modelID, meshID}] = VAO;
    return 0;
}

int DefaultRenderingComponent::SetupMeshTextures(Mesh *mesh) {
    if (mesh == nullptr) {
        printf("Mesh does not exist, aborting setup\n");
        return -1;
    }

    unsigned int materialID = mesh->GetMaterialID();
    Material *material = GetMaterial(materialID);
    if (material == nullptr) {
        printf("Material does not exist, aborting setup\n");
        return -1;
    }

    const std::vector<unsigned int> &texIDs = material->GetTextureIDs();
    for (unsigned int i = 0; i < texIDs.size(); ++i) {
        unsigned int texID = texIDs[i];
        Texture *texture = GetTexture(texID);

        GLuint textureObj;
        int setupTextureError = SetupTexture(textureObj, texture);
        if (setupTextureError) {
            printf("Failed to set up texture %d, skipping", texture->GetID());
            continue;
        }

        glBindTextureUnit(i, textureObj);
    }

    return 0;
}

int DefaultRenderingComponent::SetupTexture(GLuint &textureObj, Texture *texture) {
    if (texture == nullptr) {
        printf("Texture does not exist, aborting setup\n");
        return -1;
    }

    unsigned int textureID = texture->GetID();
    if (m_TextureObj.count(textureID) != 0) {
        textureObj = m_TextureObj[textureID];
        return 0;
    }

    texture->Load();

    glCreateTextures(GL_TEXTURE_2D, 1, &textureObj);

    glTextureParameteri(textureObj, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(textureObj, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTextureParameteri(textureObj, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTextureParameteri(textureObj, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    int texWidth = texture->GetWidth();
    int texHeight = texture->GetHeight();
    GLenum internalFormat = texture->GetInternalFormat();
    GLenum sizedInternalFormat = texture->GetSizedInternalFormat();

    glTextureStorage2D(textureObj, 3, sizedInternalFormat, texWidth, texHeight);
    glTextureSubImage2D(textureObj, 0, 0, 0, texWidth, texHeight, internalFormat, GL_UNSIGNED_BYTE,
                        texture->GetData());

    glGenerateTextureMipmap(textureObj);

    texture->Unload();

    // TODO(cluntraru): encapsulate
    m_TextureObj[textureID] = textureObj;
    return 0;
}

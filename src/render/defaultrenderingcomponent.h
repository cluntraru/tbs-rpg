#ifndef SRC_RENDER_DEFAULTRENDERINGCOMPONENT_H__
#define SRC_RENDER_DEFAULTRENDERINGCOMPONENT_H__

#include <render/renderingcomponent.h>

#include <map>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <drawable.h>

class Camera;
class Entity;
class GLFWwindow;
class Model;
class ShaderProgram;
class Slot;
class World;

class DefaultRenderingComponent: public RenderingComponent {
 public:
    explicit DefaultRenderingComponent(World *world);
    virtual ~DefaultRenderingComponent();

    int Init() override;
    void Start() override;
    void Update(float deltaTime) override;
    void Stop() override;
    void Destroy() override;

    int DrawModel(Model *const &model) override;

 private:
    void DrawEntities(World *const &world);
    void DrawSlotOverlays(World *const &world);
    int DrawMesh(Model *const &model, Mesh *const &mesh);
    int SetupMesh(GLuint &VAO, Model *const &model, Mesh *const &mesh);
    int SetupMeshTextures(Mesh *mesh);
    int SetupTexture(GLuint &textureObj, Texture *texture);

    Drawable GenerateOverlayDrawable(World *const world, const Slot *const &slot);

    // m_MeshVAO[{modelID, meshID}] = VAO
    std::map<std::pair<unsigned int, unsigned int>, GLuint> m_MeshVAO;
    std::map<unsigned int, GLuint> m_TextureObj;

    ShaderProgram *m_ShaderProgram;
    ShaderProgram *m_OverlayShaderProgram;

    Model *m_OverlayModel;

    float m_EndGameDeltaTime = 0.f;

    glm::vec3 m_AmbientAlbedo = glm::vec3(0.5f);

    static const float s_TransparentAlpha;
    static const glm::vec3 &s_InitialAmbientAlbedo;
    static const glm::vec3 &s_EndGameAmbientAlbedo;
};

#endif  // SRC_RENDER_DEFAULTRENDERINGCOMPONENT_H__


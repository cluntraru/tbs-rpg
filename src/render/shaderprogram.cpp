#include <render/shaderprogram.h>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <cstdio>
#include <string>

void ShaderProgram::SetUnifBool(const unsigned int &location, bool value) {
    glUniform1i(location, value);
}

void ShaderProgram::SetUnifInt(const unsigned int &location, int value) {
    glUniform1i(location, value);
}

void ShaderProgram::SetUnifFloat(const unsigned int &location, float value) {
    glUniform1f(location, value);
}

void ShaderProgram::SetUnifVec2(const unsigned int &location, const glm::vec2 &value) {
    glUniform2fv(location, 1, &value[0]);
}

void ShaderProgram::SetUnifVec3(const unsigned int &location, const glm::vec3 &value) {
    glUniform3fv(location, 1, &value[0]);
}

void ShaderProgram::SetUnifVec4(const unsigned int &location, const glm::vec4 &value) {
    glUniform4fv(location, 1, &value[0]);
}

void ShaderProgram::SetUnifMat2(const unsigned int &location, const glm::mat2 &mat) {
    glUniformMatrix2fv(location, 1, GL_FALSE, &mat[0][0]);
}

void ShaderProgram::SetUnifMat3(const unsigned int &location, const glm::mat3 &mat) {
    glUniformMatrix3fv(location, 1, GL_FALSE, &mat[0][0]);
}

void ShaderProgram::SetUnifMat4(const unsigned int &location, const glm::mat4 &mat) {
    glUniformMatrix4fv(location, 1, GL_FALSE, &mat[0][0]);
}

ShaderProgram::ShaderProgram() {
}

int ShaderProgram::Init(const std::string &vertexShaderPath,
                        const std::string &fragmentShaderPath) {
    GLchar *vertexShaderSource;
    vertexShaderSource = LoadShader(vertexShaderPath);

    GLchar *fragmentShaderSource;
    fragmentShaderSource = LoadShader(fragmentShaderPath);

    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShaderID, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShaderID);
    PrintShaderCompileLog(vertexShaderID);

    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(fragmentShaderID, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShaderID);
    PrintShaderCompileLog(fragmentShaderID);


    m_ProgramID = glCreateProgram();
    glAttachShader(m_ProgramID, vertexShaderID);
    glAttachShader(m_ProgramID, fragmentShaderID);
    glLinkProgram(m_ProgramID);
    PrintProgramLinkLog();

    delete[] vertexShaderSource;
    delete[] fragmentShaderSource;

    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);
    return 0;
}

void ShaderProgram::Use() const {
    glUseProgram(m_ProgramID);
}

void ShaderProgram::PrintShaderCompileLog(GLuint shader) const {
    GLint success;
    GLint logLength;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    GLchar *infoLog = new GLchar[logLength]();

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (!success) {
        glGetShaderInfoLog(shader, logLength, NULL, infoLog);
        printf("Shader compile error: \n%s", infoLog);
    }
}

void ShaderProgram::PrintProgramLinkLog() const {
    GLint success;
    GLint logLength;

    glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &logLength);
    GLchar *infoLog = new GLchar[logLength]();

    glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &success);

    if (!success) {
        glGetProgramInfoLog(m_ProgramID, logLength, NULL, infoLog);
        printf("Shader program link error: \n%s", infoLog);
    }
}

GLchar *ShaderProgram::LoadShader(const std::string &shaderPath) {
    FILE *fin = std::fopen(shaderPath.c_str(), "rb");
    if (fin == nullptr) {
        return nullptr;
    }

    std::fseek(fin, 0, SEEK_END);
    int shaderLength = static_cast<int>(std::ftell(fin));
    std::fseek(fin, 0, SEEK_SET);

    GLchar *shaderSource = new GLchar[shaderLength + 1]();
    std::fread(shaderSource, sizeof(GLchar), shaderLength, fin);
    shaderSource[shaderLength] = 0;

    fclose(fin);
    return shaderSource;
}


#ifndef SRC_RENDER_VERTEX_H__
#define SRC_RENDER_VERTEX_H__

#include <glm/glm.hpp>

class Vertex {
 public:
    Vertex(const glm::vec4 &position, const glm::vec4 &normal, const glm::vec2 &textureCoord);
    Vertex(const glm::vec3 &position, const glm::vec3 &normal, const glm::vec2 &textureCoord);
    ~Vertex();

    glm::vec4 m_Position;
    glm::vec4 m_Normal;
    glm::vec2 m_TextureCoord;

    inline const Vertex &operator =(const Vertex &other) {
        m_Position = other.m_Position;
        m_TextureCoord = other.m_TextureCoord;
        return *this;
    }
};

#endif  // SRC_RENDER_VERTEX_H__

#ifndef SRC_WORLD_H__
#define SRC_WORLD_H__

#include <list>
#include <string>
#include <vector>

#include <asset/assetloader.h>
#include <gameplay/gameplaycomponent.h>
#include <transform.h>

class Camera;
class ControlsComponent;
class DataProvider;
class Drawable;
class Entity;
class GLFWWindowData;
class RenderingComponent;

class World {
 public:
    enum ControlsType {
        DEFAULT_CONTROLS,
        GAMEPAD_CONTROLS
    };

    enum RenderingType {
        DEFAULT_RENDERING
    };

    enum AssetLoaderType {
        ASSIMP_LOADER
    };

    enum GameplayType {
        GAMEPLAY_GO,
        GAMEPLAY_CHESS
    };

    enum DataProviderType {
        MANUAL_DATAPROVIDER,
        JSON_DATAPROVIDER
    };

    World(const std::string &gameName = "chess",
          ControlsType controlsType = GAMEPAD_CONTROLS,
          RenderingType renderingType = DEFAULT_RENDERING,
          AssetLoaderType assetLoaderType = ASSIMP_LOADER,
          GameplayType gameplayType = GAMEPLAY_CHESS,
          DataProviderType dataProviderType = JSON_DATAPROVIDER);

    virtual ~World();

    inline Camera *GetCamera() const { return m_Camera; }

    virtual int Init();
    virtual void Start();
    virtual void Update();
    virtual void Stop();
    virtual void Destroy();

    virtual bool GetShouldEnd() const { return m_ShouldEnd; }
    virtual void SetShouldEnd(const bool &shouldEnd) { m_ShouldEnd = shouldEnd; }

    inline RenderingComponent *GetRenderingComponent() const { return m_RenderingComponent; }
    inline ControlsComponent *GetControlsComponent() const { return m_ControlsComponent; }
    inline AssetLoader *GetAssetLoader() const { return m_AssetLoader; }
    inline GameplayComponent *GetGameplayComponent() const { return m_GameplayComponent; }
    inline DataProvider *GetDataProvider() const { return m_DataProvider; }

    inline const std::list<Entity *> &GetRootEntities() const { return m_RootEntities; }
    // Returns array of all entities in world converted to world space
    std::vector<Drawable> FlattenEntities();
    Drawable FlattenEntity(Entity *entity);

    void AddRootEntity(Entity *entity);
    void RemoveRootEntity(Entity *entity);

 private:
    void FlattenChildren(std::vector<Drawable> &flatEntities, const glm::mat4 &parentChainTransform,
                         Entity *entity) const;

    std::list<Entity *> m_RootEntities;

    Camera *m_Camera;

    std::string m_GameName;

    RenderingComponent *m_RenderingComponent;
    ControlsComponent *m_ControlsComponent;
    AssetLoader *m_AssetLoader;
    GameplayComponent *m_GameplayComponent;
    DataProvider *m_DataProvider;

    float m_PreviousFrameTime;
    float m_DeltaTime = 0.f;

    bool m_ShouldEnd;
};

#endif  // SRC_WORLD_H__

#ifndef SRC_CAMERA_H__
#define SRC_CAMERA_H__

#include <glm/glm.hpp>

#include <transform.h>

class Camera {
 public:
    enum TranslateType {
        UP,
        RIGHT,
        FORWARD
    };

    enum RotateType {
        PITCH,
        YAW,
        ROLL
    };

    Camera();
    explicit Camera(const glm::vec4 &position);
    Camera(const glm::vec4 &position, const glm::vec4 &direction);

    ~Camera();

    inline glm::mat4 GetProjectionMatrix() const { return m_Projection; }
    glm::mat4 CalcViewMatrix() const;

    void Translate(TranslateType translateType, const float &meters);
    void Rotate(RotateType rotateType, const float &degrees);
    void InitPerspectiveMatrix();

 private:
    static const float &s_PitchLimit;
    static const float &s_FrustumNear;
    static const float &s_FrustumFar;

    float GetAspectRatio() const;

    float CalcPitch(float candidatePitch) const;

    glm::mat4 m_Projection;
    Transform m_Transform;
};

#endif  // SRC_CAMERA_H__

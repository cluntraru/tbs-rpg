#include <iostream>

#include <camera.h>
#include <controls/controlscomponent.h>
#include <render/renderingcomponent.h>
#include <window/displaycontext.h>
#include <world.h>

int main(int argc, char **argv) {
    DisplayContext *displayContext = DisplayContext::GetInstance();

    std::string controlsTypeStr = "gamepad";
    std::string gameName = "chess";
    World::ControlsType controlsType = World::GAMEPAD_CONTROLS;
    World::GameplayType gameplayType = World::GAMEPLAY_CHESS;
    if (argc >= 2) {
        controlsTypeStr = argv[1];
        if (controlsTypeStr == "gamepad") {
            controlsType = World::GAMEPAD_CONTROLS;
        } else if (controlsTypeStr == "mnk") { // Mouse and keyboard
            controlsType = World::DEFAULT_CONTROLS;
        }
    }

    if (argc >= 3) {
        gameName = argv[2];
        if (gameName == "chess") {
            gameplayType = World::GAMEPLAY_CHESS;
        } else {
            gameplayType = World::GAMEPLAY_GO;
        }
    }

    World *world = new World(gameName, controlsType, World::DEFAULT_RENDERING, World::ASSIMP_LOADER,
            gameplayType, World::JSON_DATAPROVIDER);

    displayContext->SetActiveWorld(world);

    int displayContextError = displayContext->Init();
    if (displayContextError) {
        std::cout << "DisplayContext failed to initialize\n";
        return -1;
    }

    int worldError = world->Init();
    if (worldError) {
        std::cout << "World failed to initialize\n";
        return -1;
    }

    world->Start();
    while (!world->GetShouldEnd()) {
        world->Update();
    }

    world->Stop();
    world->Destroy();

    displayContext->Destroy();
    return 0;
}


#include <controls/gamepadcontrolscomponent.h>

#include <iostream>

#include <GLFW/glfw3.h>

#include <camera.h>
#include <gameplay/gameplaycomponent.h>
#include <window/displaycontext.h>
#include <window/glfwwindowdata.h>
#include <world.h>

float GamepadControlsComponent::s_MetersPerSecond = 30.f;
const float GamepadControlsComponent::s_AxisEps = 0.2f;
const float GamepadControlsComponent::s_RotationDegreesPerSecond = 35.f;

GamepadControlsComponent::GamepadControlsComponent(World *const &world): ControlsComponent(world) {
    m_ButtonAvailable = new bool[BTN_COUNT];
    for (int i = 0; i < BTN_COUNT; ++i) {
        m_ButtonAvailable[i] = true;
    }
}

GamepadControlsComponent::~GamepadControlsComponent() {
    delete[] m_ButtonAvailable;
}

int GamepadControlsComponent::Init() {
    World *world = GetWorld();
    if (world == nullptr) {
        std::cout << "controls world error\n";
        return -1;
    }

    ControlsCallbacks::g_Camera = GetWorld()->GetCamera();
    if (ControlsCallbacks::g_Camera == nullptr) {
        return -1;
    }

    int glfwError = InitGLFWControls(world);
    if (glfwError) {
        std::cout << "controls glfw error\n";
        return glfwError;
    }

    return 0;
}

void GamepadControlsComponent::Start() {
}

void GamepadControlsComponent::Update(float deltaTime) {
    int joystickPresent = glfwJoystickPresent(GLFW_JOYSTICK_1);
    if (!joystickPresent) {
        return;
    }

    int buttonCount;
    const unsigned char *buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &buttonCount);

    int axisCount;
    const float *axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &axisCount);

    if (buttons[BTN_START] == GLFW_PRESS && IsButtonAvailable(BTN_START)) {
        GetWorld()->SetShouldEnd(true);
        SetButtonAvailable(BTN_START, false);
    }

    GameplayComponent *gameplayComponent = GetWorld()->GetGameplayComponent();
    if (buttons[BTN_SELECT] == GLFW_PRESS && IsButtonAvailable(BTN_SELECT)) {
        gameplayComponent->DeclareGameEnd();
    }

    if (buttons[BTN_A] == GLFW_PRESS && IsButtonAvailable(BTN_A)) {
        gameplayComponent->ConfirmSelection();
    }

    if (buttons[BTN_B] == GLFW_PRESS && IsButtonAvailable(BTN_B)) {
        gameplayComponent->CancelMovementConfirmation();
    }

    if (buttons[BTN_Y] == GLFW_PRESS && IsButtonAvailable(BTN_Y)) {
        gameplayComponent->PassTurn();
    }

    if (buttons[BTN_DPAD_U] == GLFW_PRESS && IsButtonAvailable(BTN_DPAD_U)) {
        gameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_UP);
    }

    if (buttons[BTN_DPAD_R] == GLFW_PRESS && IsButtonAvailable(BTN_DPAD_R)) {
        gameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_RIGHT);
    }

    if (buttons[BTN_DPAD_D] == GLFW_PRESS && IsButtonAvailable(BTN_DPAD_D)) {
        gameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_DOWN);
    }

    if (buttons[BTN_DPAD_L] == GLFW_PRESS && IsButtonAvailable(BTN_DPAD_L)) {
        gameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_LEFT);
    }

    if (buttons[BTN_LB] == GLFW_PRESS && IsButtonAvailable(BTN_LB)) {
        gameplayComponent->AdvancePlaceablePiece(-1);
    }

    if (buttons[BTN_RB] == GLFW_PRESS && IsButtonAvailable(BTN_RB)) {
        gameplayComponent->AdvancePlaceablePiece(1);
    }

    for (int i = 0; i < BTN_COUNT; ++i) {
        if (buttons[i] == GLFW_RELEASE) {
            SetButtonAvailable(static_cast<ButtonType>(i), true);
        } else {
            SetButtonAvailable(static_cast<ButtonType>(i), false);
        }
    }

    float movementMeters = s_MetersPerSecond * deltaTime;
    Camera *camera = GetWorld()->GetCamera();
    if (IsAxisActive(axes[AXIS_LOX])) {
        camera->Translate(Camera::RIGHT, movementMeters * axes[AXIS_LOX]);
    }

    if (IsAxisActive(axes[AXIS_LOY])) {
        camera->Translate(Camera::UP, -movementMeters * axes[AXIS_LOY]);
    }

    float normalizedRT = (axes[AXIS_RT] + 1.f) / 2.f;
    if (IsAxisActive(axes[AXIS_RT])) {
        camera->Translate(Camera::FORWARD, normalizedRT);
    }

    float normalizedLT = (axes[AXIS_LT] + 1.f) / 2.f;
    if (IsAxisActive(axes[AXIS_LT])) {
        camera->Translate(Camera::FORWARD, -normalizedLT);
    }

    float rotationDegrees = s_RotationDegreesPerSecond * deltaTime;
    if (IsAxisActive(axes[AXIS_ROX])) {
        camera->Rotate(Camera::YAW, -rotationDegrees * axes[AXIS_ROX]);
    }

    if (IsAxisActive(axes[AXIS_ROY])) {
        camera->Rotate(Camera::PITCH, -rotationDegrees * axes[AXIS_ROY]);
    }
}

void GamepadControlsComponent::Stop() {

}

void GamepadControlsComponent::Destroy() {

}

int GamepadControlsComponent::InitGLFWControls(World *const &world) {
    GLFWWindowData *windowData = DisplayContext::GetInstance()->GetWindowData();
    if (windowData == nullptr) {
        std::cout << "No window data in controls component\n";
        return -1;
    }

    GLFWwindow *glfwWindow = windowData->GetWindow();
    if (glfwWindow == nullptr) {
        std::cout << "No window in window data\n";
        return -1;
    }

    glfwInitHint(GLFW_JOYSTICK_HAT_BUTTONS, GLFW_FALSE);
    glfwSetFramebufferSizeCallback(glfwWindow, ControlsCallbacks::FrameBufferSizeCallback);
    return 0;
}
#ifndef SRC_CONTROLS_GAMEPADCONTROLSCOMPONENT_H__
#define SRC_CONTROLS_GAMEPADCONTROLSCOMPONENT_H__

#include <controls/controlscomponent.h>

#include <map>
#include <cmath>

class Camera;
class GameplayComponent;

class GamepadControlsComponent: public ControlsComponent {
 public:
    enum ButtonType {
        BTN_A,
        BTN_B,
        BTN_X,
        BTN_Y,
        BTN_LB,
        BTN_RB,
        BTN_SELECT,
        BTN_START,
        BTN_NONE,
        BTN_L3,
        BTN_R3,
        BTN_DPAD_U,
        BTN_DPAD_R,
        BTN_DPAD_D,
        BTN_DPAD_L,
        BTN_COUNT
    };

    enum AxisType {
        AXIS_LOX,
        AXIS_LOY,
        AXIS_LT,
        AXIS_ROX,
        AXIS_ROY,
        AXIS_RT,
        AXIS_COUNT
    };

    explicit GamepadControlsComponent(World *const &world);
    virtual ~GamepadControlsComponent();

    int Init() override;
    void Start() override;
    void Update(float deltaTime) override;
    void Stop() override;
    void Destroy() override;

 private:
    int InitGLFWControls(World *const &world);
    inline bool IsButtonAvailable(ButtonType buttonType) const {
        return m_ButtonAvailable[buttonType];
    }

    inline bool IsAxisActive(const float &axisVal) const { return fabs(axisVal) >= s_AxisEps; }

    inline void SetButtonAvailable(ButtonType buttonType, bool isAvailable) {
        m_ButtonAvailable[buttonType] = isAvailable;
    }

    bool *m_ButtonAvailable;

    static float s_MetersPerSecond;
    static const float s_AxisEps;
    static const float s_RotationDegreesPerSecond;
};

#endif  // SRC_CONTROLS_GAMEPADCONTROLSCOMPONENT_H__

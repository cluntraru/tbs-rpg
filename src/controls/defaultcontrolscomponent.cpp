#include <controls/defaultcontrolscomponent.h>

#include <iostream>
#include <cmath>

#include <GLFW/glfw3.h>

#include <world.h>
#include <camera.h>
#include <gameplay/gameplaycomponent.h>
#include <window/glfwwindowdata.h>
#include <window/displaycontext.h>

float DefaultControlsCallbackHelpers::g_MetersPerScroll = 1.f;
float DefaultControlsCallbackHelpers::g_Radius = 250.f;

void DefaultControlsCallbackHelpers::SetCameraMouseDistance(const float &distance) {
    g_Radius = distance;
}

float DefaultControlsCallbackHelpers::CameraDegreesOffsetFromAxisOffset(const float &offset) {
    const float &offsetSq = offset * offset;
    const float &radiusSq = g_Radius * g_Radius;
    return glm::degrees(glm::asin(offset * std::sqrt(4 * radiusSq - offsetSq) / (2 * radiusSq)));
}

const glm::vec2 &DefaultControlsCallbacks::g_DefaultMousePos = glm::vec2(FLT_MIN, FLT_MIN);
GameplayComponent *DefaultControlsCallbacks::g_GameplayComponent = nullptr;
glm::vec2 DefaultControlsCallbacks::g_LastPressedMouse = g_DefaultMousePos;

void DefaultControlsCallbacks::MouseCallback(GLFWwindow *window, double xPos, double yPos) {
    int lmb = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (lmb == GLFW_PRESS) {
        if (g_LastPressedMouse == g_DefaultMousePos) {
            g_LastPressedMouse = glm::vec2(xPos, yPos);
            return;
        }

        const float &pitchDegs = DefaultControlsCallbackHelpers::CameraDegreesOffsetFromAxisOffset(
                                 g_LastPressedMouse.y - yPos);

        const float &yawDegs = DefaultControlsCallbackHelpers::CameraDegreesOffsetFromAxisOffset(
                               g_LastPressedMouse.x - xPos);

        ControlsCallbacks::g_Camera->Rotate(Camera::PITCH, pitchDegs);
        ControlsCallbacks::g_Camera->Rotate(Camera::YAW, yawDegs);

        g_LastPressedMouse = glm::vec2(xPos, yPos);
    } else {
        g_LastPressedMouse = g_DefaultMousePos;
    }
}

void DefaultControlsCallbacks::ScrollCallback(GLFWwindow *window, double xOffset, double yOffset) {
    int meters = yOffset * DefaultControlsCallbackHelpers::g_MetersPerScroll;
    ControlsCallbacks::g_Camera->Translate(Camera::FORWARD, meters);
}

void DefaultControlsCallbacks::KeyCallback(GLFWwindow *window, int key, int scancode, int action,
                                           int mods) {
    if (action == GLFW_RELEASE) {
        return;
    }

    DefaultControlsComponent::InputModeType inputMode = DefaultControlsComponent::GetInputMode();
    if (key == GLFW_KEY_TAB ||
        (key == GLFW_KEY_SPACE && inputMode == DefaultControlsComponent::INPUT_CAMERA)) {

        DefaultControlsComponent::SwitchInputMode();
    }

    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS) {
        g_GameplayComponent->DeclareGameEnd();
    }

    if (inputMode == DefaultControlsComponent::INPUT_SELECTION) {
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
            g_GameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_UP);
        } else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
            g_GameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_DOWN);
        }

        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            g_GameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_LEFT);
        } else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            g_GameplayComponent->MoveSelection(GameplayComponent::MOVEMENT_RIGHT);
        }

        if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
            g_GameplayComponent->PassTurn();
        }

        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
            g_GameplayComponent->ConfirmSelection();
        }

        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
            g_GameplayComponent->CancelMovementConfirmation();
        }

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
            g_GameplayComponent->AdvancePlaceablePiece(1);
        }

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
            g_GameplayComponent->AdvancePlaceablePiece(-1);
        }
    }
}

float DefaultControlsComponent::s_MetersPerSecond = 20.f;
DefaultControlsComponent::InputModeType DefaultControlsComponent::s_InputMode = INPUT_CAMERA;
float DefaultControlsComponent::s_MoveSpeed = 5.f;

void DefaultControlsComponent::SwitchInputMode() {
    s_InputMode = (s_InputMode == INPUT_CAMERA) ? INPUT_SELECTION : INPUT_CAMERA;
}

DefaultControlsComponent::DefaultControlsComponent(World *world): ControlsComponent(world) {
}

DefaultControlsComponent::~DefaultControlsComponent() {
}

int DefaultControlsComponent::Init() {
    SetMoveSpeed(20.f);

    ControlsCallbacks::g_Camera = GetWorld()->GetCamera();
    DefaultControlsCallbacks::g_GameplayComponent = GetWorld()->GetGameplayComponent();
    if (ControlsCallbacks::g_Camera == nullptr) {
        return -1;
    }

    World *world = GetWorld();
    if (world == nullptr) {
        std::cout << "controls world error\n";
        return -1;
    }

    int glfwError = InitGLFWControls(world);
    if (glfwError) {
        std::cout << "controls glfw error\n";
        return glfwError;
    }

    return 0;
}

void DefaultControlsComponent::Start() {
}

void DefaultControlsComponent::Update(float deltaTime) {
    World *world = GetWorld();
    GLFWWindowData *windowData = DisplayContext::GetInstance()->GetWindowData();
    GLFWwindow *glfwWindow = windowData->GetWindow();

    if (glfwGetKey(glfwWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS &&
        glfwGetKey(glfwWindow, GLFW_KEY_Q) == GLFW_PRESS) {

        GetWorld()->SetShouldEnd(true);
    }

    Camera *camera = world->GetCamera();
    if (camera == nullptr) {
        return;
    }

    float movementMeters = s_MetersPerSecond * deltaTime;

    if (s_InputMode == INPUT_CAMERA) {  // Resolve camera
        if (glfwGetKey(glfwWindow, GLFW_KEY_W) == GLFW_PRESS) {
            camera->Translate(Camera::UP, movementMeters);
        }

        if (glfwGetKey(glfwWindow, GLFW_KEY_S) == GLFW_PRESS) {
            camera->Translate(Camera::UP, -movementMeters);
        }

        if (glfwGetKey(glfwWindow, GLFW_KEY_A) == GLFW_PRESS) {
            camera->Translate(Camera::RIGHT, -movementMeters);
        }

        if (glfwGetKey(glfwWindow, GLFW_KEY_D) == GLFW_PRESS) {
            camera->Translate(Camera::RIGHT, movementMeters);
        }
    }
}

void DefaultControlsComponent::Stop() {
}

void DefaultControlsComponent::Destroy() {
}

void DefaultControlsComponent::SetMoveSpeed(const float &moveSpeed) {
    s_MoveSpeed = moveSpeed;
    DefaultControlsCallbackHelpers::g_MetersPerScroll = s_MoveSpeed;
    s_MetersPerSecond = s_MoveSpeed * 4.f;
}

int DefaultControlsComponent::InitGLFWControls(World *world) {
    GLFWWindowData *windowData = DisplayContext::GetInstance()->GetWindowData();
    if (windowData == nullptr) {
        std::cout << "No window data in controls component\n";
        return -1;
    }

    GLFWwindow *glfwWindow = windowData->GetWindow();
    if (glfwWindow == nullptr) {
        std::cout << "No window in window data\n";
        return -1;
    }

    glfwSetFramebufferSizeCallback(glfwWindow, ControlsCallbacks::FrameBufferSizeCallback);
    glfwSetCursorPosCallback(glfwWindow, DefaultControlsCallbacks::MouseCallback);
    glfwSetScrollCallback(glfwWindow, DefaultControlsCallbacks::ScrollCallback);
    glfwSetKeyCallback(glfwWindow, DefaultControlsCallbacks::KeyCallback);
    return 0;
}

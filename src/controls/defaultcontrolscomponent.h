#ifndef SRC_CONTROLS_DEFAULTCONTROLSCOMPONENT_H__
#define SRC_CONTROLS_DEFAULTCONTROLSCOMPONENT_H__

#include <controls/controlscomponent.h>

#include <glm/glm.hpp>


class Camera;
class GameplayComponent;

namespace DefaultControlsCallbackHelpers {
    extern float g_MetersPerScroll;
    // Needs to be > 0
    extern float g_Radius;
    // Effectively sets pointer speed
    void SetCameraMouseDistance(const float &distance);
    // Offset needs to be in range [-radius * sqrt(2), +radius * sqrt(2)]
    float CameraDegreesOffsetFromAxisOffset(const float &offset);
}  // namespace DefaultControlsCallbackHelpers

namespace DefaultControlsCallbacks {
    extern const glm::vec2 &g_DefaultMousePos;
    extern GameplayComponent *g_GameplayComponent;
    extern glm::vec2 g_LastPressedMouse;

    void MouseCallback(GLFWwindow *window, double xpos, double ypos);
    void ScrollCallback(GLFWwindow *window, double xoffset, double yoffset);
    void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
}  // namespace DefaultControlsCallbacks

class DefaultControlsComponent: public ControlsComponent {
 public:
    enum InputModeType {
        INPUT_CAMERA,
        INPUT_SELECTION
    };

    explicit DefaultControlsComponent(World *world);
    virtual ~DefaultControlsComponent();

    int Init() override;
    void Start() override;
    void Update(float deltaTime) override;
    void Stop() override;
    void Destroy() override;

    static inline float GetMoveSpeed() { return s_MoveSpeed; }
    static void SetMoveSpeed(const float &moveSpeed);

    // Switches controls between camera and slot selection
    static void SwitchInputMode();
    static inline InputModeType GetInputMode() { return s_InputMode; }

 private:
    int InitGLFWControls(World *world);

    static float s_MetersPerSecond;
    static InputModeType s_InputMode;
    static float s_MoveSpeed;
};

#endif  // SRC_CONTROLS_DEFAULTCONTROLSCOMPONENT_H__


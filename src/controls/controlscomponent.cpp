#include <controls/controlscomponent.h>

#include <window/displaycontext.h>
#include <window/glfwwindowdata.h>

Camera *ControlsCallbacks::g_Camera = nullptr;

void ControlsCallbacks::FrameBufferSizeCallback(GLFWwindow *window, int width, int height) {
    DisplayContext *displayContext = DisplayContext::GetInstance();
    if (displayContext == nullptr) {
        return;
    }

    GLFWWindowData *windowData = displayContext->GetWindowData();
    if (windowData == nullptr) {
        return;
    }

    windowData->SetSize(width, height);
    g_Camera->InitPerspectiveMatrix();
}

ControlsComponent::ControlsComponent(World *world): Component(world) {
}

ControlsComponent::~ControlsComponent() {
}

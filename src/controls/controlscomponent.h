#ifndef SRC_CONTROLS_CONTROLSCOMPONENT_H__
#define SRC_CONTROLS_CONTROLSCOMPONENT_H__

#include <component.h>
#include <camera.h>

class GLFWwindow;

namespace ControlsCallbacks {
    extern Camera *g_Camera;
    void FrameBufferSizeCallback(GLFWwindow *window, int width, int height);
}

class ControlsComponent: public Component {
 public:
    explicit ControlsComponent(World *world);
    virtual ~ControlsComponent();
};

#endif  // SRC_CONTROLS_CONTROLSCOMPONENT_H__


#ifndef SRC_GAMES_GO_GOGAMEPLAYCOMPONENT_H__
#define SRC_GAMES_GO_GOGAMEPLAYCOMPONENT_H__

#include <gameplay/gameplaycomponent.h>

class Board;

class GoGameplayComponent: public GameplayComponent {
 public:
    enum DirectionType {
        DIRECTION_N,
        DIRECTION_S,
        DIRECTION_E,
        DIRECTION_W,
        DIRECTION_COUNT
    };

    explicit GoGameplayComponent(World *world);
    virtual ~GoGameplayComponent();

    bool IsLegalMove() override;
    bool IsLegalConfirmation() override;
    bool IsLegalPass() override { return true; }

    void SetupPieces() override;
    void SetupPlayerQueue() override;

    unsigned int UpdateSelection(MovementType movementType) override;
    void UpdatePlaceablePieces() override;

    Piece *ChooseMovementPiece(Slot *const &slot) override { return nullptr; }

    void OnGameStart() override;
    void OnPlacePiece() override;
    void OnTurnEnd() override;
    void OnGameEnd() override;
    void OnPassTurn() override;

 private:
    short int **CreateGameState();
    void DeleteGameState(short int **gameState);

    Slot *GetNextSlotInDirection(Slot *const slot, DirectionType directionType) const;
    Slot *GetNthSlotInDirection(Slot *const slot, DirectionType directionType, unsigned int num) const;

    std::pair<int, int> IdxToCoord(const unsigned int &idx) const;
    int CoordToIdx(const std::pair<int, int> &coord) const;
    bool IsCoordInBounds(const std::pair<int, int> &coord) const;

    bool WouldMoveCaptureStones(Player *const &player, Slot *const &slot);
    bool HasSlotLiberties(Player *const &player, Slot *const &slot);
    bool HasConnectedComponentLiberties(const std::pair<int, int> &coord, const int &playerCode,
            short int **gameState, bool **visited);

    void UpdateConnectedComponent(const std::pair<int, int> &coord, const int &playerCode,
            const short int &newVal, short int **gameState);

    std::pair<int, int> CalcPlayerScores();
    int EmptyAreaScore(std::pair<int, int> coord, int &playerCode, short int **gameState);

    // Returns ko slot if only one is captured and nullptr otherwise
    std::vector<Slot *> GetCapturedSlots(Player *const &player);
    void CaptureStone(Piece *const &piece);

    Piece *GetPieceOnSlot(Slot *const &slot);

    Player *m_Player1;
    Player *m_Player2;

    int m_Player1Captured = 0;
    int m_Player2Captured = 0;

    bool m_PreviousPassed = false;
    Slot *m_KoSlot = nullptr;

    static const unsigned int s_BoardWidth;
    static const unsigned int s_BoardHeight;
};

#endif  // SRC_GAMES_GO_GOGAMEPLAYCOMPONENT_H__

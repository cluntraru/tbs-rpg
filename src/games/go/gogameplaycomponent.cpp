#include <games/go/gogameplaycomponent.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include <data/dataprovider.h>
#include <gameplay/gameplaycomponent.h>
#include <gameplay/piece.h>
#include <gameplay/player.h>
#include <gameplay/slot.h>
#include <world.h>

const unsigned int GoGameplayComponent::s_BoardWidth = 13;
const unsigned int GoGameplayComponent::s_BoardHeight = 13;

GoGameplayComponent::GoGameplayComponent(World *world): GameplayComponent(world) {
}

GoGameplayComponent::~GoGameplayComponent() {
}

void GoGameplayComponent::SetupPieces() {
    World *const &world = GetWorld();
    DataProvider *const &dataProvider = world->GetDataProvider();

    std::vector<Slot *> boardSlots = m_Board->GetSlots();
    auto pieceSlots = dataProvider->GetPieceSpawnLocation();

    for (std::pair<std::string, unsigned int> pieceSlot : pieceSlots) {
        Piece *piece = nullptr;
        const std::string &modelPath = dataProvider->GetPieceModelPath(pieceSlot.first);

        if (pieceSlot.first == "darkstone") {
            piece = new Piece(world);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightstone") {
            piece = new Piece(world);
            piece->AddPlayer(m_Player1);
        }

        if (piece != nullptr) {
            piece->m_Transform.SetScale(dataProvider->GetPieceScale());
            piece->LoadModel(modelPath);
            boardSlots[pieceSlot.second]->AddPiece(piece);
        } else {
            printf("Piece is nullptr, skippping initialization\n");
        }
    }

    // Force preload of models to avoid lag on first placement
    Piece *piece = new Piece(world);
    piece->LoadModel(dataProvider->GetPieceModelPath("darkstone"));
    piece->LoadModel(dataProvider->GetPieceModelPath("lightstone"));
    delete piece;
}

void GoGameplayComponent::SetupPlayerQueue() {
    m_Player1 = new Player(1);
    m_Player2 = new Player(2);
    QueuePlayer(m_Player2);
}

bool GoGameplayComponent::IsLegalMove() {
    return false;
}

bool GoGameplayComponent::IsLegalConfirmation() {
    Player *activePlayer = GetActivePlayer();
    Slot *selectedSlot = GetSelectedSlot();
    auto pieces = selectedSlot->GetPieces();

    // Illegal if there are confirmed pieces on slot
    for (Piece *const &piece : pieces) {
        if (!piece->GetIsChosen()) {
            return false;
        }
    }

    // Ko
    if (m_KoSlot && m_KoSlot->GetID() == GetSelectedSlotIdx()) {
        return false;
    }

    // Suicide
    if (!HasSlotLiberties(activePlayer, selectedSlot)) {
        bool flag = WouldMoveCaptureStones(activePlayer, selectedSlot);
        return flag;
    }

    return true;
}

void GoGameplayComponent::UpdatePlaceablePieces() {
    World *world = GetWorld();
    DataProvider *dataProvider = world->GetDataProvider();

    DeletePlaceablePieces();

    Piece *piece = new Piece(world);
    if (GetActivePlayer() == m_Player1) {
        piece->AddPlayer(m_Player1);
        piece->LoadModel(dataProvider->GetPieceModelPath("lightstone"));
    } else {
        piece->AddPlayer(m_Player2);
        piece->LoadModel(dataProvider->GetPieceModelPath("darkstone"));
    }

    piece->m_Transform.SetScale(dataProvider->GetPieceScale());
    AddPlaceablePiece(piece);
}

unsigned int GoGameplayComponent::UpdateSelection(GameplayComponent::MovementType movementType) {
    unsigned int selectedSlotIdx = GetSelectedSlotIdx();
    unsigned int boardSize = m_Board->GetSize();
    switch (movementType) {
    case MOVEMENT_LEFT:
        selectedSlotIdx = (boardSize + selectedSlotIdx - s_BoardWidth) % boardSize;
        break;
    case MOVEMENT_UP:
        if ((selectedSlotIdx + 1) % s_BoardWidth == 0) {
            selectedSlotIdx -= s_BoardWidth - 1;
        } else {
            selectedSlotIdx += 1;
        }
        break;
    case MOVEMENT_RIGHT:
        selectedSlotIdx = (selectedSlotIdx + s_BoardWidth) % boardSize;
        break;
    case MOVEMENT_DOWN:
        if (selectedSlotIdx % s_BoardWidth == 0) {
            selectedSlotIdx += s_BoardWidth - 1;
        } else {
            selectedSlotIdx -= 1;
        }
        break;
    default:
        std::cout << "Unsupported movement type, skipping\n";
    }

    return selectedSlotIdx;
}

Slot *GoGameplayComponent::GetNextSlotInDirection(Slot *const slot,
        DirectionType directionType) const {
    if (slot == nullptr) {
        return nullptr;
    }

    int nxtSlotIdx = -1;
    unsigned int slotIdx = slot->GetID();

    int leftMost = slotIdx / s_BoardWidth * s_BoardWidth;
    int rightMost = leftMost + s_BoardWidth - 1;

    switch (directionType) {
    case DIRECTION_W:
        nxtSlotIdx = slotIdx - s_BoardWidth;
        return m_Board->GetSlot(nxtSlotIdx);
    case DIRECTION_E:
        nxtSlotIdx = slotIdx + s_BoardWidth;
        return m_Board->GetSlot(nxtSlotIdx);
    case DIRECTION_N:
        nxtSlotIdx = slotIdx + 1;
        if (nxtSlotIdx <= rightMost) {
            return m_Board->GetSlot(nxtSlotIdx);
        }

        return nullptr;
    case DIRECTION_S:
        nxtSlotIdx = slotIdx - 1;
        if (nxtSlotIdx >= leftMost) {
            return m_Board->GetSlot(nxtSlotIdx);
        }

        return nullptr;
    default:
        return nullptr;
    }
}

Slot *GoGameplayComponent::GetNthSlotInDirection(Slot *const slot, DirectionType directionType,
        unsigned int num) const {
    Slot *nthSlot = slot;
    for (int i = 0; i < num; ++i) {
        nthSlot = GetNextSlotInDirection(nthSlot, directionType);
    }

    return nthSlot;
}

std::pair<int, int> GoGameplayComponent::IdxToCoord(const unsigned int &idx) const {
    return {static_cast<int>(idx - idx / s_BoardWidth * s_BoardWidth),
            static_cast<int>(idx / s_BoardWidth)};
}

int GoGameplayComponent::CoordToIdx(const std::pair<int, int> &coord) const {
    if (!IsCoordInBounds(coord)) {
        return -1;
    }

    return coord.second * static_cast<int>(s_BoardWidth) + coord.first;
}

bool GoGameplayComponent::IsCoordInBounds(const std::pair<int, int> &coord) const {
    return coord.first >= 0 && coord.first < s_BoardHeight && coord.second >= 0 &&
           coord.second < s_BoardWidth;
}

bool GoGameplayComponent::WouldMoveCaptureStones(Player *const &player, Slot *const &slot) {
    Piece *piece = new Piece(GetWorld());
    piece->AddPlayer(player);
    m_Board->PlacePiece(piece, slot);

    Player *opposingPlayer = player == m_Player1 ? m_Player2 : m_Player1;
    bool wouldCapture = !GetCapturedSlots(opposingPlayer).empty();

    m_Board->RemovePiece(piece);
    delete piece;

    return wouldCapture;
}

short int **GoGameplayComponent::CreateGameState() {
    short int **gameState = new short int *[s_BoardHeight];
    for (int i = 0; i < s_BoardHeight; ++i) {
        gameState[i] = new short int[s_BoardWidth]();
    }

    for (Slot *const &slot : m_Board->GetSlots()) {
        Piece *piece = GetPieceOnSlot(slot);
        auto slotCoord = IdxToCoord(slot->GetID());

        if (piece && !piece->GetIsChosen()) {
            int playerCode = 2;
            if (piece->HasPlayer(m_Player1)) {
                playerCode = 1;
            }

            gameState[slotCoord.first][slotCoord.second] = playerCode;
        } else {
            gameState[slotCoord.first][slotCoord.second] = 0;
        }
    }

    return gameState;
}

void GoGameplayComponent::DeleteGameState(short int **gameState) {
    for (int i = 0; i < s_BoardHeight; ++i) {
        delete[] gameState[i];
    }

    delete[] gameState;
}

std::vector<Slot *> GoGameplayComponent::GetCapturedSlots(Player *const &player) {
    // 0 is blank, 1 is white, 2 is black, 3 is needs to be captured
    bool **visited = new bool *[s_BoardHeight];
    for (int i = 0; i < s_BoardHeight; ++i) {
        visited[i] = new bool[s_BoardWidth]();
    }

    short int **gameState = CreateGameState();

    int playerCode = player == m_Player1 ? 1 : 2;
    for (int i = 0; i < s_BoardHeight; ++i) {
        for (int j = 0; j < s_BoardWidth; ++j) {
            if (!visited[i][j] && gameState[i][j] == playerCode &&
                    !HasConnectedComponentLiberties({i, j}, playerCode, gameState, visited)) {
                UpdateConnectedComponent({i, j}, playerCode, 3, gameState);
            }
        }
    }

    std::vector<Slot *> captureSlots;
    for (int i = 0; i < s_BoardHeight; ++i) {
        for (int j = 0; j < s_BoardWidth; ++j) {
            if (gameState[i][j] == 3) {
                Slot *slot = m_Board->GetSlot(CoordToIdx({i, j}));
                captureSlots.push_back(slot);
            }
        }
    }

    DeleteGameState(gameState);

    for (int i = 0; i < s_BoardHeight; ++i) {
        delete[] visited[i];
    }

    delete[] visited;

    return captureSlots;
}

void GoGameplayComponent::CaptureStone(Piece *const &piece) {
    if (!piece) {
        return;
    }

    m_Board->RemovePiece(piece);
    if (piece->HasPlayer(m_Player1)) {
        m_Player2Captured++;
    } else {
        m_Player1Captured++;
    }

    delete piece;
}

bool GoGameplayComponent::HasSlotLiberties(Player *const &player, Slot *const &slot) {
    Slot *nSlot = GetNextSlotInDirection(slot, DIRECTION_N);
    Slot *eSlot = GetNextSlotInDirection(slot, DIRECTION_E);
    Slot *sSlot = GetNextSlotInDirection(slot, DIRECTION_S);
    Slot *wSlot = GetNextSlotInDirection(slot, DIRECTION_W);

    Piece *nPiece = nSlot ? GetPieceOnSlot(nSlot) : nullptr;
    Piece *ePiece = eSlot ? GetPieceOnSlot(eSlot) : nullptr;
    Piece *sPiece = sSlot ? GetPieceOnSlot(sSlot) : nullptr;
    Piece *wPiece = wSlot ? GetPieceOnSlot(wSlot) : nullptr;

    bool nLiberty = (nSlot && !nPiece) || (nSlot && nPiece && nPiece->HasPlayer(player));
    bool eLiberty = (eSlot && !ePiece) || (eSlot && ePiece && ePiece->HasPlayer(player));
    bool sLiberty = (sSlot && !sPiece) || (sSlot && sPiece && sPiece->HasPlayer(player));
    bool wLiberty = (wSlot && !wPiece) || (wSlot && wPiece && wPiece->HasPlayer(player));

    return nLiberty || eLiberty || sLiberty || wLiberty;
}

bool GoGameplayComponent::HasConnectedComponentLiberties(const std::pair<int, int> &coord,
        const int &playerCode, short int **gameState, bool **visited) {
    visited[coord.first][coord.second] = true;

    const int dir[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };
    bool hasLiberties = false;
    for (int dirIdx = 0; dirIdx < 4; ++dirIdx) {
        std::pair<int, int> nxtCoord = {coord.first + dir[dirIdx][0],
                                        coord.second + dir[dirIdx][1]};

        if (!IsCoordInBounds(nxtCoord) || visited[nxtCoord.first][nxtCoord.second]) {
            continue;
        }

        if (gameState[nxtCoord.first][nxtCoord.second] == playerCode) {
            hasLiberties = std::max(hasLiberties, HasConnectedComponentLiberties(nxtCoord,
                    playerCode, gameState, visited));
        } else if (gameState[nxtCoord.first][nxtCoord.second] == 0) {
            hasLiberties = true;
        }
    }

    return hasLiberties;
}

void GoGameplayComponent::UpdateConnectedComponent(const std::pair<int, int> &coord,
        const int &playerCode, const short int &newVal, short int **gameState) {
    gameState[coord.first][coord.second] = newVal;

    const int dir[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };
    for (int dirIdx = 0; dirIdx < 4; ++dirIdx) {
        std::pair<int, int> nxtCoord = {coord.first + dir[dirIdx][0],
                                        coord.second + dir[dirIdx][1]};

        if (!IsCoordInBounds(nxtCoord) || gameState[nxtCoord.first][nxtCoord.second] == newVal) {
            continue;
        }

        if (gameState[nxtCoord.first][nxtCoord.second] == playerCode) {
            UpdateConnectedComponent(nxtCoord, playerCode, newVal, gameState);
        }
    }
}

Piece *GoGameplayComponent::GetPieceOnSlot(Slot *const &slot) {
    if (!slot) {
        return nullptr;
    }

    auto slotPieces = slot->GetPieces();
    if (slotPieces.empty()) {
        return nullptr;
    }

    for (Piece *const &piece : slotPieces) {
        if (!piece->GetIsChosen()) {
            return piece;
        }
    }

    return nullptr;
}

int GoGameplayComponent::EmptyAreaScore(std::pair<int, int> coord, int &playerCode,
        short int **gameState) {
    gameState[coord.first][coord.second] = 3;

    const int dir[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    // This position
    int score = 1;
    for (int dirIdx = 0; dirIdx < 4; ++dirIdx) {
        std::pair<int, int> nxtCoord = {coord.first + dir[dirIdx][0],
                                        coord.second + dir[dirIdx][1]};
        if (!IsCoordInBounds(nxtCoord)) {
            continue;
        }

        int nxtState = gameState[nxtCoord.first][nxtCoord.second];

        // Finding a neighbour stone dictates area color
        if (playerCode == -1 && (nxtState == 1 || nxtState == 2)) {
            playerCode = gameState[nxtCoord.first][nxtCoord.second];
        }

        // Whole area is worth 0
        if ((playerCode == 1 && nxtState == 2) || (playerCode == 2 && nxtState == 1)) {
            playerCode = -2;
        }

        if (nxtState == 0) {
            score += EmptyAreaScore(nxtCoord, playerCode, gameState);
        }
    }

    return score;
}

std::pair<int, int> GoGameplayComponent::CalcPlayerScores() {
    int p1Score = 0;
    int p2Score = 0;

    short int **gameState = CreateGameState();

    for (int i = 0; i < s_BoardHeight; ++i) {
        for (int j = 0; j < s_BoardWidth; ++j) {
            if (gameState[i][j] == 1) {
                p1Score++;
            } else if (gameState[i][j] == 2) {
                p2Score++;
            }
        }
    }

    for (int i = 0; i < s_BoardHeight; ++i) {
        for (int j = 0; j < s_BoardWidth; ++j) {
            if (gameState[i][j] == 0) {
                int playerCode = -1;
                int score = EmptyAreaScore({i, j}, playerCode, gameState);
                if (playerCode == 1) {
                    p1Score += score;
                } else if (playerCode == 2) {
                    p2Score += score;
                }
            }
        }
    }

    DeleteGameState(gameState);

    return {p1Score, p2Score};
}

void GoGameplayComponent::OnGameStart() {
    SetConfirmationType(CONFIRM_PLACE);
}

void GoGameplayComponent::OnPlacePiece() {
    m_PreviousPassed = false;
    Player *activePlayer = GetActivePlayer();
    Player *opposingPlayer = activePlayer == m_Player1 ? m_Player2 : m_Player1;

    auto opposingSlotsCaptured = GetCapturedSlots(opposingPlayer);
    if (opposingSlotsCaptured.size() == 1) {
        m_KoSlot = opposingSlotsCaptured.front();
    } else {
        m_KoSlot = nullptr;
    }

    for (Slot *const &slot : opposingSlotsCaptured) {
        Piece *piece = GetPieceOnSlot(slot);
        CaptureStone(piece);
    }

    auto activeSlotsCaptured = GetCapturedSlots(activePlayer);
    for (Slot *const &slot : activeSlotsCaptured) {
        Piece *piece = GetPieceOnSlot(slot);
        CaptureStone(piece);
    }

    SetShouldAdvanceTurn(true);
}

void GoGameplayComponent::OnTurnEnd() {
    unsigned int activePlayerId = GetActivePlayer()->GetId();
    Player *nxtPlayer = (activePlayerId == 1) ? m_Player2 : m_Player1;
    QueuePlayer(nxtPlayer);
}

void GoGameplayComponent::OnPassTurn() {
    if (m_PreviousPassed) {
        SetShouldGameEnd(true);
    }

    m_KoSlot = nullptr;
    m_PreviousPassed = true;
}

void GoGameplayComponent::OnGameEnd() {
    std::pair<int, int> scores = CalcPlayerScores();
    if (scores.first >= scores.second) {
        AddWinner(m_Player1);
    }

    if (scores.second >= scores.first) {
        AddWinner(m_Player2);
    }
}

#ifndef SRC_GAMES_CHESS_CHESSPIECE_H__
#define SRC_GAMES_CHESS_CHESSPIECE_H__

#include <gameplay/piece.h>

#include <vector>

class World;

class ChessPiece: public Piece {
 public:
    enum PieceType {
        PAWN,
        ROOK,
        KNIGHT,
        BISHOP,
        QUEEN,
        KING
    };

    ChessPiece(World *const &world, PieceType pieceType);
    virtual ~ChessPiece();

    inline PieceType GetPieceType() { return m_PieceType; }

    inline bool GetIsMoved() const { return m_IsMoved; }
    inline void SetIsMoved(const bool &isMoved) { m_IsMoved = isMoved; }

    inline bool GetIsVulnerableToEnPassant() const { return m_IsVulnerableToEnPassant; }
    inline void SetIsVulnerableToEnPassant(const bool &isVulnerableToEnPassant) {
        m_IsVulnerableToEnPassant = isVulnerableToEnPassant;
    }

 private:
    PieceType m_PieceType;

    bool m_IsMoved = false;
    bool m_IsVulnerableToEnPassant = false;
};

#endif  // SRC_GAMES_CHESS_CHESSPIECE_H__

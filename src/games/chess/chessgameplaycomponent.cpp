#include <games/chess/chessgameplaycomponent.h>

#include <algorithm>
#include <list>
#include <string>
#include <utility>
#include <vector>

#include <data/dataprovider.h>
#include <gameplay/piece.h>
#include <gameplay/player.h>
#include <gameplay/slot.h>
#include <games/chess/chesspiece.h>
#include <world.h>
#include <iostream>

const unsigned int ChessGameplayComponent::s_BoardWidth = 8;
const unsigned int ChessGameplayComponent::s_BoardHeight = 8;

ChessGameplayComponent::ChessGameplayComponent(World *world): GameplayComponent(world) {
}

ChessGameplayComponent::~ChessGameplayComponent() {
}

void ChessGameplayComponent::SetupPieces() {
    World *const &world = GetWorld();
    DataProvider *const &dataProvider = world->GetDataProvider();

    std::vector<Slot *> boardSlots = m_Board->GetSlots();
    auto pieceSlots = dataProvider->GetPieceSpawnLocation();

    for (std::pair<std::string, unsigned int> pieceSlot : pieceSlots) {
        ChessPiece *piece = nullptr;
        const std::string &modelPath = dataProvider->GetPieceModelPath(pieceSlot.first);

        if (pieceSlot.first == "darkpawn") {
            piece = new ChessPiece(world, ChessPiece::PAWN);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightpawn") {
            piece = new ChessPiece(world, ChessPiece::PAWN);
            piece->AddPlayer(m_Player1);
        } else if (pieceSlot.first == "darkrook") {
            piece = new ChessPiece(world, ChessPiece::ROOK);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightrook") {
            piece = new ChessPiece(world, ChessPiece::ROOK);
            piece->AddPlayer(m_Player1);
        } else if (pieceSlot.first == "darkknight") {
            piece = new ChessPiece(world, ChessPiece::KNIGHT);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightknight") {
            piece = new ChessPiece(world, ChessPiece::KNIGHT);
            piece->AddPlayer(m_Player1);
        } else if (pieceSlot.first == "darkbishop") {
            piece = new ChessPiece(world, ChessPiece::BISHOP);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightbishop") {
            piece = new ChessPiece(world, ChessPiece::BISHOP);
            piece->AddPlayer(m_Player1);
        } else if (pieceSlot.first == "darkking") {
            piece = new ChessPiece(world, ChessPiece::KING);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightking") {
            piece = new ChessPiece(world, ChessPiece::KING);
            piece->AddPlayer(m_Player1);
        } else if (pieceSlot.first == "darkqueen") {
            piece = new ChessPiece(world, ChessPiece::QUEEN);
            piece->AddPlayer(m_Player2);
        } else if (pieceSlot.first == "lightqueen") {
            piece = new ChessPiece(world, ChessPiece::QUEEN);
            piece->AddPlayer(m_Player1);
        }

        if (piece != nullptr) {
            piece->m_Transform.SetScale(dataProvider->GetPieceScale());
            piece->LoadModel(modelPath);
            boardSlots[pieceSlot.second]->AddPiece(piece);
        } else {
            printf("Piece is nullptr, skippping initialization\n");
        }
    }
}

void ChessGameplayComponent::SetupPlayerQueue() {
    m_Player1 = new Player(1);
    m_Player2 = new Player(2);
    QueuePlayer(m_Player1);
}

void ChessGameplayComponent::OnTurnStart() {
    // Remove en passant vulnerabilities
    for (Slot *slot : m_Board->GetSlots()) {
        if (!slot->GetPieces().empty() && slot->GetPieces().front()->HasPlayer(GetActivePlayer())) {
            ChessPiece *piece = dynamic_cast<ChessPiece *>(slot->GetPieces().front());
            piece->SetIsVulnerableToEnPassant(false);
        }
    }
}

void ChessGameplayComponent::OnTurnEnd() {
    unsigned int activePlayerId = GetActivePlayer()->GetId();
    Player *nxtPlayer = (activePlayerId == 1) ? m_Player2 : m_Player1;
    QueuePlayer(nxtPlayer);

    // Check if game is over
    if (IsCheckmate(nxtPlayer)) {
        AddWinner(GetActivePlayer());
        SetShouldGameEnd(true);
        SetShouldAdvanceTurn(true);
    }

    if (IsDraw(nxtPlayer)) {
        AddWinner(GetActivePlayer());
        AddWinner(nxtPlayer);
        SetShouldGameEnd(true);
        SetShouldAdvanceTurn(true);
    }
}

void ChessGameplayComponent::OnRoundStart() {
}

void ChessGameplayComponent::OnRoundEnd() {
}

void ChessGameplayComponent::OnGameStart() {
}

void ChessGameplayComponent::OnGameEnd() {
}

void ChessGameplayComponent::OnDeclareGameEnd() {
    AddWinner(m_Player1);
    AddWinner(m_Player2);
}

void ChessGameplayComponent::ResolveEnPassant(ChessPiece *const &pawn) {
    Slot *confirmedSlot = pawn->GetSlot();
    Slot *selectedSlot = GetSelectedSlot();

    Slot *twoAhead = nullptr;
    Slot *diagonalW = nullptr;
    Slot *diagonalE = nullptr;
    if (GetActivePlayer() == m_Player1) {  // White
        twoAhead = GetNthSlotInDirection(confirmedSlot, DIRECTION_N, 2);
        diagonalW = GetNextSlotInDirection(confirmedSlot, DIRECTION_NW);
        diagonalE = GetNextSlotInDirection(confirmedSlot, DIRECTION_NE);
    } else {  // Black
        twoAhead = GetNthSlotInDirection(confirmedSlot, DIRECTION_S, 2);
        diagonalW = GetNextSlotInDirection(confirmedSlot, DIRECTION_SW);
        diagonalE = GetNextSlotInDirection(confirmedSlot, DIRECTION_SE);
    }

    Slot *oneW = GetNextSlotInDirection(confirmedSlot, DIRECTION_W);
    Slot *oneE = GetNextSlotInDirection(confirmedSlot, DIRECTION_E);

    if (twoAhead && selectedSlot->GetID() == twoAhead->GetID()) {
        pawn->SetIsVulnerableToEnPassant(true);
    } else if (diagonalW && selectedSlot->GetID() == diagonalW->GetID() && diagonalW->GetPieces().empty()) {
        CapturePiece(oneW);
    } else if (diagonalE && selectedSlot->GetID() == diagonalE->GetID() && diagonalE->GetPieces().empty()) {
        CapturePiece(oneE);
    }
}

void ChessGameplayComponent::ResolvePromotion(ChessPiece *&pawn) {
    Slot *selectedSlot = GetSelectedSlot();
    std::pair<int, int> selectedSlotCoord = IdxToCoord(selectedSlot->GetID());
    if (selectedSlotCoord.first != 0 && selectedSlotCoord.first != 7) {  // Not a promotion
        return;
    }

    m_PromotionSlot = selectedSlot;
    CapturePiece(pawn->GetSlot());
    pawn = nullptr;
    SetConfirmationType(CONFIRM_PLACE);
}

void ChessGameplayComponent::UpdatePlaceablePieces() {
    World *world = GetWorld();
    DataProvider *dataProvider = world->GetDataProvider();
    DeletePlaceablePieces();

    if (GetActivePlayer() == m_Player2) {  // Black
        std::vector<std::pair<std::string, ChessPiece::PieceType>> promotionPieces = {
                {"darkrook",   ChessPiece::ROOK},
                {"darkknight", ChessPiece::KNIGHT},
                {"darkbishop", ChessPiece::BISHOP},
                {"darkqueen",  ChessPiece::QUEEN}
        };

        for (const auto &pieceData : promotionPieces) {
            ChessPiece *piece = new ChessPiece(world, pieceData.second);
            piece->AddPlayer(m_Player2);
            piece->LoadModel(dataProvider->GetPieceModelPath(pieceData.first));
            piece->m_Transform.SetScale(dataProvider->GetPieceScale());
            AddPlaceablePiece(piece);
        }
    } else {  // White
        std::vector<std::pair<std::string, ChessPiece::PieceType>> promotionPieces = {
                {"lightrook",   ChessPiece::ROOK},
                {"lightknight", ChessPiece::KNIGHT},
                {"lightbishop", ChessPiece::BISHOP},
                {"lightqueen",  ChessPiece::QUEEN}
        };

        for (const auto &pieceData : promotionPieces) {
            ChessPiece *piece = new ChessPiece(world, pieceData.second);
            piece->AddPlayer(m_Player1);
            piece->LoadModel(dataProvider->GetPieceModelPath(pieceData.first));
            piece->m_Transform.SetScale(dataProvider->GetPieceScale());
            AddPlaceablePiece(piece);
        }
    }
}

void ChessGameplayComponent::OnPlaceMovement() {
    Slot *selectedSlot = GetSelectedSlot();

    Slot *confirmedSlot = GetConfirmedSlot();
    ChessPiece *pieceToMove = dynamic_cast<ChessPiece *>(confirmedSlot->GetPieces().front());

    // Castling
    if (pieceToMove->GetPieceType() == ChessPiece::KING &&
            GetSlotDistance(confirmedSlot, selectedSlot) == 2) {
        std::pair<int, int> selectedSlotCoords = IdxToCoord(selectedSlot->GetID());
        unsigned int rookSlotIdx;
        unsigned int rookTargetSlotIdx;
        if (selectedSlotCoords.second == 2) {  // Second file
            rookSlotIdx = CoordToIdx({selectedSlotCoords.first, selectedSlotCoords.second - 2});
            rookTargetSlotIdx = CoordToIdx({selectedSlotCoords.first, selectedSlotCoords.second + 1});
        } else {  // Penultimate file
            rookSlotIdx = CoordToIdx({selectedSlotCoords.first, selectedSlotCoords.second + 1});
            rookTargetSlotIdx = CoordToIdx({selectedSlotCoords.first, selectedSlotCoords.second - 1});
        }

        ChessPiece *rook = dynamic_cast<ChessPiece *>(m_Board->GetSlot(rookSlotIdx)->GetPieces().front());
        Slot *rookTargetSlot = m_Board->GetSlot(rookTargetSlotIdx);
        m_Board->PlacePiece(rook, rookTargetSlot);
        rook->SetIsMoved(true);
    }

    if (pieceToMove->GetPieceType() == ChessPiece::PAWN) {
        ResolveEnPassant(pieceToMove);
    }

    // Capture piece
    CapturePiece(selectedSlot);

    if (pieceToMove->GetPieceType() == ChessPiece::PAWN) {
        ResolvePromotion(pieceToMove);
    }

    // Set a piece that was moved as moved
    if (pieceToMove) {
        pieceToMove->SetIsMoved(true);
    }

    // Only advance turn if not promoting after movement
    if (GetConfirmationType() == CONFIRM_MOVE) {
        SetShouldAdvanceTurn(true);
    }
}

void ChessGameplayComponent::OnPlacePlacement() {
    Slot *selectedSlot = GetSelectedSlot();
    if (selectedSlot->GetPieces().empty()) {
        std::cout << "Trying to promote pawn to nothing, skipping\n";
        SetConfirmationType(CONFIRM_MOVE);
        return;
    }

    m_PromotionSlot = nullptr;
    SetConfirmationType(CONFIRM_MOVE);
    SetShouldAdvanceTurn(true);
}

void ChessGameplayComponent::OnPlacePiece() {
    if (GetConfirmationType() == CONFIRM_MOVE) {
        OnPlaceMovement();
    } else {
        OnPlacePlacement();
    }
}

void ChessGameplayComponent::CapturePiece(Slot *const &slot) const {
    if (slot->GetPieces().empty()) {
        return;
    }

    Piece *slotPiece = slot->GetPieces().front();
    slot->RemovePiece(slotPiece);
    delete slotPiece;
}

bool ChessGameplayComponent::IsLegalMove() {
    Slot *confirmedSlot = GetConfirmedSlot();
    Slot *selectedSlot = GetSelectedSlot();
    if (!confirmedSlot) {
        std::cout << "No slot was confirmed, cannot execute movement\n";
        return false;
    }

    const std::list<Piece *> &confirmedSlotPieces = confirmedSlot->GetPieces();
    if (confirmedSlotPieces.empty()) {
        std::cout << "An empty slot was confirmed, this is illegal\n";
        return false;
    }

    std::vector<Slot *> validSlots = GetAccessibleSlots(confirmedSlotPieces.front());
    if (DoesMoveCauseSelfCheck(confirmedSlot, selectedSlot)) {
        return false;
    }

    return std::find(validSlots.begin(), validSlots.end(), selectedSlot) != validSlots.end();
}

bool ChessGameplayComponent::IsLegalConfirmation() {
    if (GetConfirmationType() == CONFIRM_MOVE) {
        Slot *slot = GetSelectedSlot();
        const std::list<Piece *> &pieces = slot->GetPieces();
        if (pieces.empty()) {
            return false;
        }

        return pieces.front()->HasPlayer(GetActivePlayer());
    } else if (m_PromotionSlot && GetSelectedSlot()->GetID() == m_PromotionSlot->GetID()) {
        return true;
    }

    return false;
}

Piece *ChessGameplayComponent::ChooseMovementPiece(Slot *const &slot) {
    Piece *chosenPiece = nullptr;
    if (GetConfirmationType() == CONFIRM_MOVE) {
        const std::list<Piece *> &slotPieces = slot->GetPieces();
        if (!slotPieces.empty()) {
            chosenPiece = slotPieces.front();
        }
    } else {
        chosenPiece = GetCurrentPlaceablePiece();
    }

    return chosenPiece;
}

unsigned int ChessGameplayComponent::UpdateSelection(GameplayComponent::MovementType movementType) {
    unsigned int selectedSlotIdx = GetSelectedSlotIdx();
    unsigned int boardSize = m_Board->GetSize();
    switch (movementType) {
     case MOVEMENT_UP:
         selectedSlotIdx = (boardSize + selectedSlotIdx - s_BoardWidth) % boardSize;
         break;
     case MOVEMENT_RIGHT:
         if ((selectedSlotIdx + 1) % s_BoardWidth == 0) {
             selectedSlotIdx -= s_BoardWidth - 1;
         } else {
             selectedSlotIdx += 1;
         }
         break;
     case MOVEMENT_DOWN:
         selectedSlotIdx = (selectedSlotIdx + s_BoardWidth) % boardSize;
         break;
     case MOVEMENT_LEFT:
         if (selectedSlotIdx % s_BoardWidth == 0) {
             selectedSlotIdx += s_BoardWidth - 1;
         } else {
             selectedSlotIdx -= 1;
         }
         break;
     default:
         std::cout << "Unsupported movement type, skipping\n";
    }

    return selectedSlotIdx;
}

Slot *ChessGameplayComponent::GetNextSlotInDirection(Slot *const slot,
        ChessGameplayComponent::DirectionType directionType) const {
    if (slot == nullptr) {
        return nullptr;
    }

    int nxtSlotIdx = -1;
    int boardSize = m_Board->GetSize();
    unsigned int slotIdx = slot->GetID();

    int leftMost = slotIdx / s_BoardWidth * s_BoardWidth;
    int rightMost = leftMost + s_BoardWidth - 1;

    int leftMostAbove = leftMost - s_BoardWidth;
    int rightMostAbove = rightMost - s_BoardWidth;

    int leftMostBelow = leftMost + s_BoardWidth;
    int rightMostBelow = rightMost + s_BoardWidth;

    switch (directionType) {
        case DIRECTION_N:
            nxtSlotIdx = slotIdx - s_BoardWidth;
            return m_Board->GetSlot(nxtSlotIdx);
        case DIRECTION_S:
            nxtSlotIdx = slotIdx + s_BoardWidth;
            return m_Board->GetSlot(nxtSlotIdx);
        case DIRECTION_E:
            nxtSlotIdx = slotIdx + 1;
            if (nxtSlotIdx <= rightMost) {
                return m_Board->GetSlot(nxtSlotIdx);
            }

            return nullptr;
        case DIRECTION_W:
            nxtSlotIdx = slotIdx - 1;
            if (nxtSlotIdx >= leftMost) {
                return m_Board->GetSlot(nxtSlotIdx);
            }

            return nullptr;
        case DIRECTION_NW:
            nxtSlotIdx = slotIdx - s_BoardWidth - 1;
            if (nxtSlotIdx >= 0 && nxtSlotIdx >= leftMostAbove) {
                return m_Board->GetSlot(nxtSlotIdx);
            }

            return nullptr;
        case DIRECTION_NE:
            nxtSlotIdx = slotIdx - s_BoardWidth + 1;
            if (nxtSlotIdx >= 0 && nxtSlotIdx <= rightMostAbove) {
                return m_Board->GetSlot(nxtSlotIdx);
            }

            return nullptr;
        case DIRECTION_SW:
            nxtSlotIdx = slotIdx + s_BoardWidth - 1;
            if (nxtSlotIdx <= boardSize && nxtSlotIdx >= leftMostBelow) {
                return m_Board->GetSlot(nxtSlotIdx);
            }

            return nullptr;
        case DIRECTION_SE:
            nxtSlotIdx = slotIdx + s_BoardWidth + 1;
            if (nxtSlotIdx <= boardSize && nxtSlotIdx <= rightMostBelow) {
                return m_Board->GetSlot(nxtSlotIdx);
            }

            return nullptr;
        default:
            return nullptr;
    }
}

Slot *ChessGameplayComponent::GetNthSlotInDirection(Slot *const slot, DirectionType directionType,
                                                    unsigned int num) const {
    Slot *nthSlot = slot;
    for (int i = 0; i < num; ++i) {
        nthSlot = GetNextSlotInDirection(nthSlot, directionType);
    }

    return nthSlot;
}

bool ChessGameplayComponent::DoesMoveCauseSelfCheck(Slot *const &srcSlot,
                                                    Slot *const &dstSlot) const {
    // Modify board state to reflect potential move
    Piece *prevSrcPiece = nullptr;
    if (srcSlot->GetPieces().empty()) {
        return false;
    }

    Player *pieceOwner = srcSlot->GetPieces().front()->GetPlayers().front();
    prevSrcPiece = srcSlot->GetPieces().front();

    Piece *prevDstPiece = nullptr;
    if (!dstSlot->GetPieces().empty()) {
        prevDstPiece = dstSlot->GetPieces().front();
        dstSlot->RemovePiece(prevDstPiece);
    }

    m_Board->PlacePiece(prevSrcPiece, dstSlot);
    bool doesCauseSelfCheck = false;
    if (IsThreatened(FindKing(pieceOwner))) {
        doesCauseSelfCheck = true;
    }

    // Revert changes
    m_Board->PlacePiece(prevSrcPiece, srcSlot);
    if (prevDstPiece) {
        dstSlot->AddPiece(prevDstPiece);
    }

    return doesCauseSelfCheck;
}

Piece *ChessGameplayComponent::FindKing(Player *const &player) const {
    for (Slot *const &slot : m_Board->GetSlots()) {
        for (Piece *piece : slot->GetPieces()) {
            ChessPiece *chessPiece = dynamic_cast<ChessPiece *>(piece);
            if (chessPiece->GetPieceType() == ChessPiece::KING &&
                    chessPiece->HasPlayer(player)) {
                return chessPiece;
            }
        }
    }

    return nullptr;
}

bool ChessGameplayComponent::IsThreatened(Piece *const &piece) const {
    if (piece == nullptr) {
        return false;
    }

    Player *pieceOwner = piece->GetPlayers().front();
    for (Slot *const &slot : m_Board->GetSlots()) {
        std::list<Piece *> slotPieces = slot->GetPieces();
        if (slotPieces.empty() || slotPieces.front()->HasPlayer(pieceOwner)) {
            continue;
        }

        ChessPiece *slotPiece = dynamic_cast<ChessPiece *>(slotPieces.front());
        std::vector<Slot *> accessibleSlots = GetAccessibleSlots(slotPiece, false);

        if (std::find(accessibleSlots.begin(), accessibleSlots.end(), piece->GetSlot()) !=
                accessibleSlots.end()) {
            return true;
        }
    }

    return false;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessibleDirectionSlots(Piece *const &piece,
    ChessGameplayComponent::DirectionType directionType) const {

    std::vector<Slot *> slots;
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr) {
        return slots;
    }

    currSlot = GetNextSlotInDirection(currSlot, directionType);
    while (currSlot != nullptr && currSlot->GetPieces().empty()) {
        slots.push_back(currSlot);
        currSlot = GetNextSlotInDirection(currSlot, directionType);
    }

    Piece *pieceOnSlot = nullptr;
    if (currSlot != nullptr) {
        pieceOnSlot = currSlot->GetPieces().front();
    }

    Player *pieceOwner = piece->GetPlayers().front();
    if (pieceOnSlot != nullptr && !pieceOnSlot->HasPlayer(pieceOwner)) {
        slots.push_back(currSlot);
    }

    return slots;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessibleSlots(Piece *const &piece, const bool &checkCastling) const {
    ChessPiece *chessPiece = dynamic_cast<ChessPiece *>(piece);
    std::vector<Slot *> validSlots;
    switch (chessPiece->GetPieceType()) {
    case ChessPiece::PAWN:
        validSlots = GetAccessiblePawnSlots(chessPiece);
        break;
    case ChessPiece::ROOK:
        validSlots = GetAccessibleRookSlots(chessPiece);
        break;
    case ChessPiece::KNIGHT:
        validSlots = GetAccessibleKnightSlots(chessPiece);
        break;
    case ChessPiece::BISHOP:
        validSlots = GetAccessibleBishopSlots(chessPiece);
        break;
    case ChessPiece::KING:
        validSlots = GetAccessibleKingSlots(chessPiece, checkCastling);
        break;
    case ChessPiece::QUEEN:
        validSlots = GetAccessibleQueenSlots(chessPiece);
        break;
    }

    return validSlots;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessiblePawnSlots(Piece *const &piece) const {
    std::vector<Slot *> slots;
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr) {
        return slots;
    }

    Player *pieceOwner = piece->GetPlayers().front();

    unsigned int slotIdx = piece->GetSlot()->GetID();
    std::pair<int, int> slotCoord = IdxToCoord(slotIdx);
    Slot *oneAhead = nullptr;
    Slot *twoAhead = nullptr;
    Slot *diagonalW = nullptr;
    Slot *diagonalE = nullptr;
    Slot *oneW = nullptr;
    Slot *oneE = nullptr;

    unsigned int startingRank;
    if (pieceOwner == m_Player1) {  // White
        oneAhead = GetNextSlotInDirection(currSlot, DIRECTION_N);
        twoAhead = GetNthSlotInDirection(currSlot, DIRECTION_N, 2);

        diagonalW = GetNextSlotInDirection(currSlot, DIRECTION_NW);
        diagonalE = GetNextSlotInDirection(currSlot, DIRECTION_NE);

        startingRank = 6;
    } else {  // Black
        oneAhead = GetNextSlotInDirection(currSlot, DIRECTION_S);
        twoAhead = GetNthSlotInDirection(currSlot, DIRECTION_S, 2);

        diagonalW = GetNextSlotInDirection(currSlot, DIRECTION_SW);
        diagonalE = GetNextSlotInDirection(currSlot, DIRECTION_SE);

        startingRank = 1;
    }

    oneW = GetNextSlotInDirection(currSlot, DIRECTION_W);
    oneE = GetNextSlotInDirection(currSlot, DIRECTION_E);

    if (oneAhead && oneAhead->GetPieces().empty()) {
        slots.push_back(oneAhead);
        if (slotCoord.first == startingRank && twoAhead && twoAhead->GetPieces().empty()) {
            slots.push_back(twoAhead);
        }
    }

    // West capturing
    Piece *diagonalWPiece = nullptr;
    if (diagonalW && !diagonalW->GetPieces().empty()) {
        diagonalWPiece = diagonalW->GetPieces().front();
    }

    ChessPiece *oneWPiece = nullptr;
    if (oneW && !oneW->GetPieces().empty()) {
        oneWPiece = dynamic_cast<ChessPiece *>(oneW->GetPieces().front());
    }

    if (diagonalWPiece && !diagonalWPiece->HasPlayer(pieceOwner)) {
        slots.push_back(diagonalW);
    } else if (oneWPiece && oneWPiece->GetIsVulnerableToEnPassant()) {
        slots.push_back(diagonalW);
    }

    // East capturing
    Piece *diagonalEPiece = nullptr;
    if (diagonalE && !diagonalE->GetPieces().empty()) {
        diagonalEPiece = diagonalE->GetPieces().front();
    }

    ChessPiece *oneEPiece = nullptr;
    if (oneE && !oneE->GetPieces().empty()) {
        oneEPiece = dynamic_cast<ChessPiece *>(oneE->GetPieces().front());
    }

    if (diagonalEPiece && !diagonalEPiece->HasPlayer(pieceOwner)) {
        slots.push_back(diagonalE);
    } else if (oneEPiece && oneEPiece->GetIsVulnerableToEnPassant()) {
        slots.push_back(diagonalE);
    }

    return slots;
}


std::vector<Slot *> ChessGameplayComponent::GetAccessibleRookSlots(Piece *const &piece) const {
    std::vector<Slot *> slots;
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr) {
        return slots;
    }

    const std::vector<Slot *> &slotsN = GetAccessibleDirectionSlots(piece, DIRECTION_N);
    const std::vector<Slot *> &slotsS = GetAccessibleDirectionSlots(piece, DIRECTION_S);
    const std::vector<Slot *> &slotsE = GetAccessibleDirectionSlots(piece, DIRECTION_E);
    const std::vector<Slot *> &slotsW = GetAccessibleDirectionSlots(piece, DIRECTION_W);

    slots.insert(slots.end(), slotsN.begin(), slotsN.end());
    slots.insert(slots.end(), slotsE.begin(), slotsE.end());
    slots.insert(slots.end(), slotsS.begin(), slotsS.end());
    slots.insert(slots.end(), slotsW.begin(), slotsW.end());

    return slots;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessibleKnightSlots(Piece *const &piece) const {
    std::vector<Slot *> slots;
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr) {
        return slots;
    }

    Player *pieceOwner = piece->GetPlayers().front();

    std::pair<int, int> currCoord = IdxToCoord(currSlot->GetID());
    const int dir[8][2] = { {-2, -1}, {-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}};
    for (int dirIdx = 0; dirIdx < 8; ++dirIdx) {
        std::pair<int, int> nxtCoord = {currCoord.first + dir[dirIdx][0],
                                        currCoord.second + dir[dirIdx][1]};

        if (!IsCoordInBounds(nxtCoord)) {
            continue;
        }

        Slot *nxtSlot = m_Board->GetSlot(CoordToIdx(nxtCoord));
        if (!nxtSlot) {
            continue;
        }

        const std::list<Piece *> &nxtSlotPieces = nxtSlot->GetPieces();
        if (nxtSlotPieces.empty() || !nxtSlotPieces.front()->HasPlayer(pieceOwner)) {
            slots.push_back(nxtSlot);
        }
    }

    return slots;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessibleBishopSlots(Piece *const &piece) const {
    std::vector<Slot *> slots;
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr) {
        return slots;
    }

    const std::vector<Slot *> &slotsNW = GetAccessibleDirectionSlots(piece, DIRECTION_NW);
    const std::vector<Slot *> &slotsNE = GetAccessibleDirectionSlots(piece, DIRECTION_NE);
    const std::vector<Slot *> &slotsSE = GetAccessibleDirectionSlots(piece, DIRECTION_SE);
    const std::vector<Slot *> &slotsSW = GetAccessibleDirectionSlots(piece, DIRECTION_SW);

    slots.insert(slots.end(), slotsNW.begin(), slotsNW.end());
    slots.insert(slots.end(), slotsNE.begin(), slotsNE.end());
    slots.insert(slots.end(), slotsSE.begin(), slotsSE.end());
    slots.insert(slots.end(), slotsSW.begin(), slotsSW.end());

    return slots;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessibleQueenSlots(Piece *const &piece) const {
    std::vector<Slot *> slots;
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr) {
        return slots;
    }

    const std::vector<Slot *> &slotsN = GetAccessibleDirectionSlots(piece, DIRECTION_N);
    const std::vector<Slot *> &slotsS = GetAccessibleDirectionSlots(piece, DIRECTION_S);
    const std::vector<Slot *> &slotsE = GetAccessibleDirectionSlots(piece, DIRECTION_E);
    const std::vector<Slot *> &slotsW = GetAccessibleDirectionSlots(piece, DIRECTION_W);

    const std::vector<Slot *> &slotsNW = GetAccessibleDirectionSlots(piece, DIRECTION_NW);
    const std::vector<Slot *> &slotsNE = GetAccessibleDirectionSlots(piece, DIRECTION_NE);
    const std::vector<Slot *> &slotsSE = GetAccessibleDirectionSlots(piece, DIRECTION_SE);
    const std::vector<Slot *> &slotsSW = GetAccessibleDirectionSlots(piece, DIRECTION_SW);

    slots.insert(slots.end(), slotsN.begin(), slotsN.end());
    slots.insert(slots.end(), slotsE.begin(), slotsE.end());
    slots.insert(slots.end(), slotsS.begin(), slotsS.end());
    slots.insert(slots.end(), slotsW.begin(), slotsW.end());

    slots.insert(slots.end(), slotsNW.begin(), slotsNW.end());
    slots.insert(slots.end(), slotsNE.begin(), slotsNE.end());
    slots.insert(slots.end(), slotsSE.begin(), slotsSE.end());
    slots.insert(slots.end(), slotsSW.begin(), slotsSW.end());

    return slots;
}

bool ChessGameplayComponent::IsCastleValid(Piece *const &piece, DirectionType directionType) const {
    if (directionType != DIRECTION_E && directionType != DIRECTION_W) {
        return false;
    }

    std::vector<Slot *> castleSlots = GetAccessibleDirectionSlots(piece, directionType);
    int slotsBetween = (directionType == DIRECTION_E) ? 2 : 3;
    bool validCastle = true;

    // Castling is not legal if any slots between are occupied
    validCastle = std::min(validCastle, castleSlots.size() == slotsBetween);
    for (Slot *currESlot : castleSlots) {
        validCastle = std::min(validCastle, currESlot->GetPieces().empty());
    }

    // Castling is not legal if moving through check
    for (int i = 0; i < 2 && i < castleSlots.size(); ++i) {
        bool isSafeFromCheck = !DoesMoveCauseSelfCheck(piece->GetSlot(), castleSlots[i]);
        validCastle = std::min(validCastle, isSafeFromCheck);
    }

    Slot *rookSlot = GetNthSlotInDirection(piece->GetSlot(), directionType, slotsBetween + 1);
    ChessPiece *rook = nullptr;
    if (rookSlot) {
        rook = dynamic_cast<ChessPiece *>(rookSlot->GetPieces().front());
    }

    if (!rook || rook->GetIsMoved()) {
        validCastle = false;
    }

    return validCastle;
}

std::vector<Slot *> ChessGameplayComponent::GetAccessibleKingSlots(Piece *const &piece, const bool &checkCastling) const {
    std::vector<Slot *> slots;
    ChessPiece *kingPiece = dynamic_cast<ChessPiece *>(piece);
    Slot *currSlot = piece->GetSlot();
    if (currSlot == nullptr || kingPiece->GetPieceType() != ChessPiece::KING) {
        return slots;
    }

    Player *pieceOwner = piece->GetPlayers().front();
    DirectionType firstDirection = static_cast<DirectionType>(0);
    for (DirectionType dType = firstDirection; dType < DIRECTION_COUNT; dType = static_cast<DirectionType>(dType + 1)) {
        Slot *nxtSlot = GetNextSlotInDirection(piece->GetSlot(), dType);
        if (!nxtSlot) {
            continue;
        }

        std::list<Piece *> slotPieces = nxtSlot->GetPieces();
        if (slotPieces.empty() || !slotPieces.front()->HasPlayer(pieceOwner)) {
            slots.push_back(nxtSlot);
        }
    }

    // Castling
    if (checkCastling && !kingPiece->GetIsMoved() && !IsThreatened(kingPiece)) {
        if (IsCastleValid(piece, DIRECTION_W)) {
            slots.push_back(GetNthSlotInDirection(currSlot, DIRECTION_W, 2));
        }

        if (IsCastleValid(piece, DIRECTION_E)) {
            slots.push_back(GetNthSlotInDirection(currSlot, DIRECTION_E, 2));
        }
    }

    return slots;
}

std::pair<int, int> ChessGameplayComponent::IdxToCoord(const unsigned int &idx) const {
    return {static_cast<int>(idx / s_BoardWidth),
            static_cast<int>(idx - idx / s_BoardWidth * s_BoardWidth)};
}

int ChessGameplayComponent::CoordToIdx(const std::pair<int, int> &coord) const {
    if (!IsCoordInBounds(coord)) {
        return -1;
    }

    return coord.first * static_cast<int>(s_BoardWidth) + coord.second;
}

bool ChessGameplayComponent::IsCoordInBounds(const std::pair<int, int> &coord) const {
    return coord.first >= 0 && coord.first < s_BoardHeight && coord.second >= 0 &&
           coord.second < s_BoardWidth;
}

unsigned int ChessGameplayComponent::GetSlotDistance(Slot *const &srcSlot, Slot *const &dstSlot) const {
    std::pair<int, int> srcCoord = IdxToCoord(srcSlot->GetID());
    std::pair<int, int> dstCoord = IdxToCoord(dstSlot->GetID());

    unsigned int verticalDist = std::abs(srcCoord.first - dstCoord.first);
    unsigned int horizontalDist = std::abs(srcCoord.second - dstCoord.second);

    return std::max(verticalDist, horizontalDist);
}

bool ChessGameplayComponent::IsCheckmate(Player *const &threatenedPlayer) const {
    return IsThreatened(FindKing(threatenedPlayer)) && NoLegalMoves(threatenedPlayer);
}

bool ChessGameplayComponent::IsDraw(Player *const &threatenedPlayer) const {
    // No legal moves draw
    return !IsThreatened(FindKing(threatenedPlayer)) && NoLegalMoves(threatenedPlayer);
}

bool ChessGameplayComponent::NoLegalMoves(Player *const &player) const {
    for (Slot *const &slot : m_Board->GetSlots()) {
        if (slot->GetPieces().empty() || !slot->GetPieces().front()->HasPlayer(player)) {
            continue;
        }

        std::vector<Slot *> accSlots = GetAccessibleSlots(slot->GetPieces().front(), false);
        for (Slot *const &accSlot : accSlots) {
            if (!DoesMoveCauseSelfCheck(slot, accSlot)) {
                return false;
            }
        }
    }

    return true;
}

Piece *ChessGameplayComponent::GetPieceOnSlot(Slot *const &slot) {
    if (!slot) {
        return nullptr;
    }

    auto slotPieces = slot->GetPieces();
    if (slotPieces.empty()) {
        return nullptr;
    }

    for (Piece *const &piece : slotPieces) {
        if (!piece->GetIsChosen()) {
            return piece;
        }
    }

    return nullptr;
}


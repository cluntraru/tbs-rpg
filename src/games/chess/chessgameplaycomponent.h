#ifndef SRC_GAMES_CHESS_CHESSGAMEPLAYCOMPONENT_H__
#define SRC_GAMES_CHESS_CHESSGAMEPLAYCOMPONENT_H__

#include <gameplay/gameplaycomponent.h>

class ChessPiece;
class World;

class ChessGameplayComponent: public GameplayComponent {
 public:
    enum DirectionType {
        DIRECTION_N,
        DIRECTION_S,
        DIRECTION_E,
        DIRECTION_W,
        DIRECTION_NW,
        DIRECTION_NE,
        DIRECTION_SW,
        DIRECTION_SE,
        DIRECTION_COUNT
    };

    explicit ChessGameplayComponent(World *world);
    virtual ~ChessGameplayComponent();

    void OnTurnStart() override;
    void OnTurnEnd() override;
    void OnRoundStart() override;
    void OnRoundEnd() override;
    void OnGameStart() override;
    void OnGameEnd() override;
    void OnDeclareGameEnd() override;
    void OnPlacePiece() override;
    void UpdatePlaceablePieces() override;
    Piece *ChooseMovementPiece(Slot *const &slot) override;

 protected:
    void SetupPieces() override;
    void SetupPlayerQueue() override;

 private:
    Slot *GetNextSlotInDirection(Slot *const slot, DirectionType directionType) const;
    Slot *GetNthSlotInDirection(Slot *const slot, DirectionType directionType, unsigned int num) const;

    Piece *FindKing(Player *const &player) const;
    bool DoesMoveCauseSelfCheck(Slot *const &srcSlot, Slot *const &dstSlot) const;
    bool IsThreatened(Piece *const &piece) const;

    std::vector<Slot *> GetAccessibleDirectionSlots(Piece *const &piece,
                                                    DirectionType directionType) const;

    std::vector<Slot *> GetAccessibleSlots(Piece *const &piece, const bool &checkCastling = true) const;
    std::vector<Slot *> GetAccessiblePawnSlots(Piece *const &piece) const;
    std::vector<Slot *> GetAccessibleRookSlots(Piece *const &piece) const;
    std::vector<Slot *> GetAccessibleKnightSlots(Piece *const &piece) const;
    std::vector<Slot *> GetAccessibleBishopSlots(Piece *const &piece) const;
    std::vector<Slot *> GetAccessibleQueenSlots(Piece *const &piece) const;
    std::vector<Slot *> GetAccessibleKingSlots(Piece *const &piece, const bool &checkCastling = true) const;
    bool IsCastleValid(Piece *const &piece, DirectionType directionType) const;

    void ResolveEnPassant(ChessPiece *const &pawn);
    void ResolvePromotion(ChessPiece *&pawn);

    void OnPlaceMovement();
    void OnPlacePlacement();

    void CapturePiece(Slot *const &slot) const;

    bool IsCheckmate(Player *const &threatenedPlayer) const;
    bool IsDraw(Player *const &threatenedPlayer) const;
    bool NoLegalMoves(Player *const &player) const;

    Piece *GetPieceOnSlot(Slot *const &slot);

    std::pair<int, int> IdxToCoord(const unsigned int &idx) const;
    int CoordToIdx(const std::pair<int, int> &coord) const;
    bool IsCoordInBounds(const std::pair<int, int> &coord) const;
    unsigned int GetSlotDistance(Slot *const &srcSlot, Slot *const &dstSlot) const;

    unsigned int UpdateSelection(MovementType movementType) override;
    bool IsLegalMove() override;
    bool IsLegalConfirmation() override;
    bool IsLegalPass() override { return false; }

    Player *m_Player1;
    Player *m_Player2;

    Slot *m_PromotionSlot;

    std::vector<std::vector<Piece *>> m_CachedStates;

    static const unsigned int s_BoardWidth;
    static const unsigned int s_BoardHeight;
};

#endif  // SRC_GAMES_CHESS_CHESSGAMEPLAYCOMPONENT_H__

#include <games/chess/chesspiece.h>

#include <gameplay/board.h>
#include <gameplay/slot.h>
#include <world.h>

ChessPiece::ChessPiece(World *const &world, PieceType pieceType): Piece(world) {
    m_PieceType = pieceType;
}

ChessPiece::~ChessPiece() {
}

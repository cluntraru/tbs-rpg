#ifndef SRC_GAMEPLAY_PIECE_H__
#define SRC_GAMEPLAY_PIECE_H__

#include <entity.h>

#include <algorithm>

#include <list>

class Slot;
class Player;

class Piece: public Entity {
 public:
    explicit Piece(World *const &world);
    virtual ~Piece();

    inline Slot *GetSlot() const { return m_Slot; }
    inline void SetSlot(Slot *slot) { m_Slot = slot; }

    inline void AddPlayer(Player *const &player) { m_Players.push_back(player); }
    inline bool HasPlayer(Player *const &player) {
        return std::find(m_Players.begin(), m_Players.end(), player) != m_Players.end();
    }
    inline std::list<Player *> GetPlayers() const { return m_Players; }

    inline void SetIsChosen(const bool &isChosen) { m_IsChosen = isChosen; }
    inline bool GetIsChosen() const { return m_IsChosen; }

 private:
    Slot *m_Slot = nullptr;
    std::list<Player *> m_Players;
    bool m_IsChosen = false;
};

#endif  // SRC_GAMEPLAY_PIECE_H__

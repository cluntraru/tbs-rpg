#ifndef SRC_GAMEPLAY_PLAYER_H__
#define SRC_GAMEPLAY_PLAYER_H__

class Player {
 public:
    explicit Player(unsigned int id);
    virtual ~Player();

    inline unsigned int GetId() const { return m_Id; }

 private:
    unsigned int m_Id;
};

#endif  // SRC_GAMEPLAY_PLAYER_H__

#ifndef SRC_GAMEPLAY_BOARD_H__
#define SRC_GAMEPLAY_BOARD_H__

#include <vector>

#include <entity.h>

class Slot;
class Piece;

class Board: public Entity {
 public:
    explicit Board(World *world);
    virtual ~Board();

    virtual void AddSlot(Slot *slot);
    inline std::vector<Slot *> GetSlots() { return m_Slots; }
    inline unsigned int GetSize() { return m_Slots.size(); }
    inline Slot *GetSlot(unsigned int idx) {
        if (idx >= 0 && idx < m_Slots.size()) {
            return m_Slots[idx];
        }

        return nullptr;
    }

    virtual void PlacePiece(Piece *piece, Slot *slot);
    virtual void RemovePiece(Piece *piece);

 private:
    std::vector<Slot *> m_Slots;
};

#endif  // SRC_GAMEPLAY_BOARD_H__

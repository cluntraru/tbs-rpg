#include <gameplay/gameplaycomponent.h>

#include <string>
#include <vector>

#include <data/dataprovider.h>
#include <gameplay/board.h>
#include <gameplay/piece.h>
#include <gameplay/slot.h>
#include <world.h>
#include <iostream>

GameplayComponent::GameplayComponent(World *world): Component(world) {
}

GameplayComponent::~GameplayComponent() {
}

int GameplayComponent::Init() {
    World *world = GetWorld();
    if (world == nullptr) {
        printf("Invalid world in gameplay component, aborting\n");
        Destroy();
        return -1;
    }

    DataProvider *dataProvider = world->GetDataProvider();
    if (dataProvider == nullptr) {
        printf("Data provider is null in gameplay component, aborting");
        Destroy();
        return -1;
    }

    const std::string &boardModelPath = dataProvider->GetBoardModelPath();
    m_Board = new Board(GetWorld());

    const glm::vec3 &boardEulerAngles = dataProvider->GetBoardEulerAngles();
    m_Board->m_Transform.ApplyPitch(boardEulerAngles.x);
    m_Board->m_Transform.ApplyYaw(boardEulerAngles.y);
    m_Board->m_Transform.ApplyRoll(boardEulerAngles.z);

    m_Board->LoadModel(boardModelPath);
    world->AddRootEntity(m_Board);

    int initSlotsError = InitSlots();
    if (initSlotsError) {
        return initSlotsError;
    }

    int initPlayerQueueError = InitPlayerQueue();
    if (initPlayerQueueError) {
        return initPlayerQueueError;
    }

    int initPiecesError = InitPieces();
    if (initPiecesError) {
        return initPiecesError;
    }

    return 0;
}

int GameplayComponent::InitSlots() {
    World *const &world = GetWorld();
    DataProvider *const &dataProvider = world->GetDataProvider();

    const std::vector<glm::vec3> &slotCoords = dataProvider->GetSlotCoords();
    for (int i = 0; i < slotCoords.size(); ++i) {
        const glm::vec3 &slotCoord = slotCoords[i];
        Slot *slot = new Slot(world, Transform(glm::vec4(slotCoord, 1.f)), i);
        m_Board->AddSlot(slot);
    }

    return 0;
}

int GameplayComponent::InitPieces() {
    SetupPieces();
    return 0;
}

int GameplayComponent::InitPlayerQueue() {
    SetupPlayerQueue();
    return 0;
}

void GameplayComponent::Start() {
    m_IsGameOver = false;
    m_ShouldGameEnd = false;
    m_SelectedSlotIdx = 0;
    m_ConfirmedSlotIdx = -1;
    Slot *firstSlot = m_Board->GetSlot(m_SelectedSlotIdx);
    UpdateOverlay();

    StartGame();
}

void GameplayComponent::Update(float deltaTime) {
}

void GameplayComponent::Stop() {
}

void GameplayComponent::Destroy() {
    GetWorld()->RemoveRootEntity(m_Board);
    delete m_Board;
}

void GameplayComponent::NextTurn() {
    m_ShouldAdvanceTurn = false;
    CancelMovementConfirmation();

    OnTurnEnd();

    if (m_ShouldAdvanceRound) {
        NextRound();
    }

    ++m_TurnNum;
    if (m_PlayerQueue.size() <= 1) {
        std::cout << "Player queue is empty, ending game";
        EndGame();
    } else {
        m_PlayerQueue.pop();
    }

    OnTurnStart();
    if (m_ConfirmationType == CONFIRM_PLACE) {
        if (m_ChosenPlacementPiece) {
            m_Board->RemovePiece(m_ChosenPlacementPiece);
        }

        UpdatePlaceablePieces();
    }

    UpdatePlacementDisplay();
    UpdateOverlay();

    if (m_ShouldGameEnd) {
        EndGame();
    }
}

void GameplayComponent::StartGame() {
    OnGameStart();
    OnRoundStart();
    OnTurnStart();
}

void GameplayComponent::EndGame() {
    m_IsGameOver = true;

    if (m_ChosenPlacementPiece) {
        m_Board->RemovePiece(m_ChosenPlacementPiece);
        m_ChosenPlacementPiece = nullptr;
        DeletePlaceablePieces();

        UpdatePlacementDisplay();
    }

    UpdateOverlay();

    OnGameEnd();

    for (Slot *const &slot : m_Board->GetSlots()) {
        bool isWinnerOnSlot = false;

        auto slotPieces = slot->GetPieces();
        for (Player *const &winner : m_Winners) {

            for (Piece *const &piece : slot->GetPieces()) {

                if (!piece->GetIsChosen() && piece->HasPlayer(winner)) {
                    isWinnerOnSlot = true;
                }
            }
        }

        if (isWinnerOnSlot) {
            slot->SetSelectedType(Slot::SELECTED_VALID);
        }
    }
}

void GameplayComponent::NextRound() {
    m_ShouldAdvanceRound = false;
    OnRoundEnd();

    ++m_RoundNum;
    OnRoundStart();
}

// Methods to be implemented by user
void GameplayComponent::OnTurnStart() {
}

void GameplayComponent::OnTurnEnd() {
}

void GameplayComponent::OnRoundStart() {
}

void GameplayComponent::OnRoundEnd() {
}

void GameplayComponent::OnGameStart() {
}

void GameplayComponent::OnGameEnd() {
}

void GameplayComponent::OnDeclareGameEnd() {
}

void GameplayComponent::OnPlacePiece() {
}

void GameplayComponent::OnPassTurn() {
}

void GameplayComponent::UpdatePlaceablePieces() {
}

void GameplayComponent::RemovePlaceablePiece(Piece *const &piece) {
    m_PlaceablePiecesIdx = 0;
    auto it = std::find(m_PlaceablePieces.begin(), m_PlaceablePieces.end(), piece);
    if (it != m_PlaceablePieces.end()) {
        m_PlaceablePieces.erase(it);
    }
}

void GameplayComponent::DeletePlaceablePiece(Piece *const &piece) {
    m_PlaceablePiecesIdx = 0;
    auto it = std::find(m_PlaceablePieces.begin(), m_PlaceablePieces.end(), piece);
    if (it != m_PlaceablePieces.end()) {
        delete *it;
        m_PlaceablePieces.erase(it);
    }
}

void GameplayComponent::DeletePlaceablePieces() {
    m_PlaceablePiecesIdx = 0;
    for (Piece *piece : m_PlaceablePieces) {
        delete piece;
    }

    m_PlaceablePieces.clear();
}

Piece *GameplayComponent::GetCurrentPlaceablePiece() const {
    if (m_PlaceablePiecesIdx >= m_PlaceablePieces.size()) {
        return nullptr;
    }

    auto it = m_PlaceablePieces.begin();
    std::advance(it, m_PlaceablePiecesIdx);
    return *it;
}

void GameplayComponent::AdvancePlaceablePiece(int offset) {
    if (m_PlaceablePieces.empty()) {
        m_PlaceablePiecesIdx = 0;
        return;
    }

    m_PlaceablePiecesIdx += offset + m_PlaceablePieces.size();
    m_PlaceablePiecesIdx %= m_PlaceablePieces.size();
    UpdatePlacementDisplay();
}

void GameplayComponent::MoveSelection(GameplayComponent::MovementType movementType) {
    if (IsGameOver()) {
        return;
    }

    unsigned int candidateSelectedSlotIdx = UpdateSelection(movementType);
    Slot *candidateSelectedSlot = m_Board->GetSlot(candidateSelectedSlotIdx);
    if (candidateSelectedSlot == nullptr) {
        std::cout << "Slot index " << candidateSelectedSlotIdx << " is not valid, ignoring\n";
        return;
    }

    m_SelectedSlotIdx = candidateSelectedSlotIdx;

    if (GetConfirmationType() == CONFIRM_PLACE) {
        UpdatePlacementDisplay();
    }

    UpdateOverlay();
}

void GameplayComponent::ConfirmSelection() {
    if (IsGameOver()) {
        return;
    }

    Slot *selectedSlot = m_Board->GetSlot(m_SelectedSlotIdx);

    if (m_ConfirmationType == CONFIRM_MOVE) {
        if (m_ConfirmedSlotIdx == -1 && IsLegalConfirmation()) {
            m_ConfirmedSlotIdx = m_SelectedSlotIdx;
            UpdateChosenPiece();
        } else if (m_ConfirmedSlotIdx == m_SelectedSlotIdx) {
            CancelMovementConfirmation();
        } else if (m_ConfirmedSlotIdx != -1 && IsLegalMove()) {
            if (m_ChosenPiece != nullptr) {
                OnPlacePiece();
                if (m_ChosenPiece) {
                    m_Board->PlacePiece(m_ChosenPiece, selectedSlot);
                }
            }

            CancelMovementConfirmation();
            if (m_ShouldAdvanceTurn) {
                NextTurn();
            }
        }
    } else {
        if (m_ChosenPlacementPiece != nullptr && IsLegalConfirmation()) {
            RemovePlaceablePiece(m_ChosenPlacementPiece);
            m_ChosenPlacementPiece->SetIsChosen(false);  // Confirm placement
            m_ChosenPlacementPiece = nullptr;
            OnPlacePiece();

            if (m_ShouldAdvanceTurn) {
                NextTurn();
            } else {
                UpdatePlaceablePieces();
                UpdateChosenPiece();
            }
        }
    }

    UpdateOverlay();
}

void GameplayComponent::DeclareGameEnd() {
    if (IsGameOver()) {
        return;
    }

    OnDeclareGameEnd();
    SetShouldGameEnd(true);
    NextTurn();
}

void GameplayComponent::PassTurn() {
    if (IsGameOver()) {
        return;
    }

    if (IsLegalPass()) {
        OnPassTurn();
        NextTurn();
    }
}

void GameplayComponent::CancelMovementConfirmation() {
    ClearChosenMovementPiece();
    m_ConfirmedSlotIdx = -1;
    UpdateOverlay();
}

void GameplayComponent::SetConfirmationType(GameplayComponent::ConfirmationType confirmationType) {
    if (m_ConfirmationType == CONFIRM_MOVE) {
        CancelMovementConfirmation();
    }

    m_ConfirmationType = confirmationType;

    Slot *selectedSlot = m_Board->GetSlot(m_SelectedSlotIdx);
    if (m_ConfirmationType == CONFIRM_MOVE) {
    } else if (m_ConfirmationType == CONFIRM_PLACE) {
        UpdatePlaceablePieces();
        UpdateChosenPiece();
    }

    UpdatePlacementDisplay();
    UpdateOverlay();
}

void GameplayComponent::CycleConfirmationType() {
    ConfirmationType confirmationType = static_cast<ConfirmationType>(
                                            (m_ConfirmationType + 1) % CONFIRM_COUNT);

    SetConfirmationType(confirmationType);
}

void GameplayComponent::UpdateChosenPiece() {
    if (m_ConfirmationType == CONFIRM_MOVE) {
        Slot *selectedSlot = m_Board->GetSlot(m_SelectedSlotIdx);
        m_ChosenPiece = ChooseMovementPiece(selectedSlot);

        if (m_ChosenPiece) {
            m_ChosenPiece->SetIsChosen(true);
        }
    } else {
        m_ChosenPlacementPiece = GetCurrentPlaceablePiece();
    }
}

void GameplayComponent::ClearChosenMovementPiece() {
    if (m_ChosenPiece) {
        m_ChosenPiece->SetIsChosen(false);
    }

    m_ChosenPiece = nullptr;
}

void GameplayComponent::UpdateOverlay() {
    for (Slot *slot : m_SelectedSlots) {
        slot->SetSelectedType(Slot::DESELECTED);
    }

    m_SelectedSlots.clear();

    if (IsGameOver()) {  // Only reset display if game is ongoing
        return;
    }

    Slot *selectedSlot = GetSelectedSlot();
    if (selectedSlot == nullptr) {
        std::cout << "No selected slot, exiting\n";
        exit(-1);
    }

    Slot *confirmedSlot = GetConfirmedSlot();
    if (m_ConfirmationType == CONFIRM_MOVE) {
        if (confirmedSlot == nullptr && IsLegalConfirmation()) {
            selectedSlot->SetSelectedType(Slot::SELECTED_VALID);
        } else if (confirmedSlot != nullptr && IsLegalMove()) {
            selectedSlot->SetSelectedType(Slot::SELECTED_VALID);
        } else {
            selectedSlot->SetSelectedType(Slot::SELECTED_INVALID);
        }

        if (confirmedSlot != nullptr) {
            confirmedSlot->SetSelectedType(Slot::CONFIRMED);
        }
    } else {
        if (IsLegalConfirmation()) {
            selectedSlot->SetSelectedType(Slot::SELECTED_VALID);
        } else {
            selectedSlot->SetSelectedType(Slot::SELECTED_INVALID);
        }
    }

    m_SelectedSlots.push_back(selectedSlot);

    if (confirmedSlot != nullptr) {
        m_SelectedSlots.push_back(confirmedSlot);
    }
}

void GameplayComponent::UpdatePlacementDisplay() {
    if (m_ChosenPlacementPiece) {
        m_Board->RemovePiece(m_ChosenPlacementPiece);
    }

    if (GetConfirmationType() == CONFIRM_PLACE && IsLegalConfirmation()) {
        m_ChosenPlacementPiece = GetCurrentPlaceablePiece();
        if (m_ChosenPlacementPiece) {
            Slot *selectedSlot = m_Board->GetSlot(m_SelectedSlotIdx);
            m_Board->PlacePiece(m_ChosenPlacementPiece, selectedSlot);
            m_ChosenPlacementPiece->SetIsChosen(true);
        }
    }
}

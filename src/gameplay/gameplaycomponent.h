#ifndef SRC_GAMEPLAY_GAMEPLAYCOMPONENT_H__
#define SRC_GAMEPLAY_GAMEPLAYCOMPONENT_H__

#include <component.h>

#include <algorithm>
#include <queue>

#include <gameplay/board.h>

class Piece;
class Player;
class Slot;

class GameplayComponent: public Component {
 public:
    enum MovementType {
        MOVEMENT_UP,
        MOVEMENT_LEFT,
        MOVEMENT_DOWN,
        MOVEMENT_RIGHT
    };

    enum ConfirmationType {
        CONFIRM_MOVE,
        CONFIRM_PLACE,
        CONFIRM_COUNT
    };

    explicit GameplayComponent(World *world);
    virtual ~GameplayComponent();

    int Init() override;
    void Start() override;
    void Update(float deltaTime) override;
    void Stop() override;
    void Destroy() override;

    void MoveSelection(MovementType movementType);
    void ConfirmSelection();
    void PassTurn();
    void DeclareGameEnd();

    inline bool IsGameOver() { return m_IsGameOver; }

    inline Board *GetBoard() const { return m_Board; }

    void SetConfirmationType(ConfirmationType confirmationType);
    inline ConfirmationType GetConfirmationType() const { return m_ConfirmationType; }
    void CycleConfirmationType();

    inline void QueuePlayer(Player *player) { m_PlayerQueue.push(player); }
    inline Player *GetActivePlayer() const { return m_PlayerQueue.front(); }

    void CancelMovementConfirmation();

    void AdvancePlaceablePiece(int offset = 1);
    Piece *GetCurrentPlaceablePiece() const;

 protected:
    // Methods to be implemented by user
    virtual void OnTurnStart();
    virtual void OnTurnEnd();
    virtual void OnRoundStart();
    virtual void OnRoundEnd();
    virtual void OnGameStart();
    virtual void OnGameEnd();
    virtual void OnDeclareGameEnd();
    virtual void OnPlacePiece();
    virtual void OnPassTurn();

    virtual void UpdatePlaceablePieces();

    virtual bool IsLegalMove() = 0;
    virtual bool IsLegalConfirmation() = 0;
    virtual bool IsLegalPass() = 0;

    virtual void SetupPieces() = 0;
    virtual void SetupPlayerQueue() = 0;

    virtual Piece *ChooseMovementPiece(Slot *const &slot) = 0;

    inline unsigned int GetConfirmedSlotIdx() const { return m_ConfirmedSlotIdx; }
    inline Slot *GetConfirmedSlot() const { return m_Board->GetSlot(m_ConfirmedSlotIdx); }

    inline unsigned int GetSelectedSlotIdx() const { return m_SelectedSlotIdx; }
    inline Slot *GetSelectedSlot() const { return m_Board->GetSlot(m_SelectedSlotIdx); }
    virtual unsigned int UpdateSelection(MovementType movementType) = 0;

    inline void AddWinner(Player *const &player) { m_Winners.push_back(player); }

    bool GetShouldAdvanceTurn() const { return m_ShouldAdvanceTurn; }
    void SetShouldAdvanceTurn(const bool &shouldAdvanceTurn) { m_ShouldAdvanceTurn = shouldAdvanceTurn; }

    bool GetShouldAdvanceRound() const { return m_ShouldAdvanceRound; }
    void SetShouldAdvanceRound(const bool &shouldAdvanceRound) { m_ShouldAdvanceRound = shouldAdvanceRound; }

    inline const std::list<Piece *> &GetPlaceablePieces() { return m_PlaceablePieces; }
    inline void AddPlaceablePiece(Piece *const &piece) { m_PlaceablePieces.push_back(piece); }
    void RemovePlaceablePiece(Piece *const &piece);
    void DeletePlaceablePiece(Piece *const &piece);
    void DeletePlaceablePieces();

    void SetShouldGameEnd(const bool &shouldGameEnd) {  m_ShouldGameEnd = shouldGameEnd; }

    Board *m_Board;

 private:
    void NextTurn();
    void StartGame();
    void EndGame();
    void NextRound();

    int InitSlots();
    int InitPieces();
    int InitPlayerQueue();

    void UpdateChosenPiece();
    void ClearChosenMovementPiece();

    void UpdatePlacementDisplay();
    void UpdateOverlay();

    ConfirmationType m_ConfirmationType = CONFIRM_MOVE;
    Piece *m_ChosenPiece = nullptr;
    Piece *m_ChosenPlacementPiece = nullptr;

    std::queue<Player *> m_PlayerQueue;
    std::list<Slot *> m_SelectedSlots;

    std::list<Piece *> m_PlaceablePieces;
    unsigned int m_PlaceablePiecesIdx = 0;

    // Pieces on board belonging to these players will be highlighted at end of game
    std::vector<Player *> m_Winners;

    int m_TurnNum = 0;
    int m_RoundNum = 0;
    bool m_IsGameOver = false;

    bool m_ShouldAdvanceTurn = false;
    bool m_ShouldAdvanceRound = false;
    bool m_ShouldGameEnd = false;

    // -1 means nothing is confirmed, otherwise it is the index in board of confirmed slot
    int m_ConfirmedSlotIdx = -1;
    unsigned int m_SelectedSlotIdx = 0;
};

#endif  // SRC_GAMEPLAY_GAMEPLAYCOMPONENT_H__

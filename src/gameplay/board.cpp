#include <gameplay/board.h>

#include <iostream>

#include <gameplay/piece.h>
#include <gameplay/slot.h>

Board::Board(World *world): Entity(world) {
}

Board::~Board() {
}

void Board::AddSlot(Slot *slot) {
    AddChild(slot);

    m_Slots.push_back(slot);
    slot->SetBoard(this);
}

void Board::PlacePiece(Piece *piece, Slot *slot) {
    if (piece == nullptr || slot == nullptr) {
        std::cout << "Cannot place piece, piece or slot is null\n";
        return;
    }

    Slot *oldSlot = piece->GetSlot();
    if (oldSlot != nullptr) {
        oldSlot->RemovePiece(piece);
    }

    slot->AddPiece(piece);
}

void Board::RemovePiece(Piece *piece) {
    if (piece == nullptr) {
        std::cout << "Cannot remove piece, piece does not exist\n";
        return;
    }

    Slot *slot = piece->GetSlot();
    if (slot) {
        slot->RemovePiece(piece);
    }
}

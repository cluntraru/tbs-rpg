#ifndef SRC_GAMEPLAY_SLOT_H__
#define SRC_GAMEPLAY_SLOT_H__

#include <entity.h>

#include <list>

class Board;
class Piece;

class Slot: public Entity {
 public:
    enum SelectedType {
        SELECTED_VALID,
        SELECTED_INVALID,
        CONFIRMED,
        DESELECTED
    };

    explicit Slot(World *const &world, const Transform &transform, const unsigned int &ID);
    virtual ~Slot();

    inline const std::list<Piece *> &GetPieces() const { return m_Pieces; }

    void AddPiece(Piece *piece);
    void RemovePiece(Piece *piece);

    inline unsigned int GetID() { return m_ID; }

    inline Board *GetBoard() const { return m_Board; }
    inline void SetBoard(Board *const &board) { m_Board = board; }

    inline SelectedType GetSelectedType() { return m_SelectedType; }
    inline void SetSelectedType(SelectedType selectedType) { m_SelectedType = selectedType; }

 private:
    std::list<Piece *> m_Pieces;

    Board *m_Board = nullptr;

    SelectedType m_SelectedType = DESELECTED;

    unsigned int m_ID;
};

#endif  // SRC_GAMEPLAY_SLOT_H__

#include <gameplay/slot.h>

#include <algorithm>

#include <gameplay/piece.h>

Slot::Slot(World *const &world, const Transform &transform, const unsigned int &ID)
        : Entity(world, transform) {
    m_ID = ID;
}

Slot::~Slot() {
}

void Slot::AddPiece(Piece *piece) {
    AddChild(piece);

    m_Pieces.push_back(piece);
    piece->SetSlot(this);
}

void Slot::RemovePiece(Piece *piece) {
    RemoveChild(piece);

    m_Pieces.remove(piece);
    piece->SetSlot(nullptr);
}

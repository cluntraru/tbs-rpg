#ifndef SRC_ASSET_MODEL_H__
#define SRC_ASSET_MODEL_H__

#include <vector>
#include <map>

class Mesh;

class Model {
 public:
    explicit Model(const unsigned int &ID);
    virtual ~Model();

    inline unsigned int GetID() const { return m_ID; }
    inline const std::vector<Mesh *> &GetMeshes() const { return m_Meshes; }

    Mesh *RegisterMesh();
    void DeregisterMeshes();

 private:
    unsigned int m_ID;
    unsigned int m_LastRegisteredMeshID = 0;

    std::vector<Mesh *> m_Meshes;
};

#endif  // SRC_ASSET_MODEL_H__


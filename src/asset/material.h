#ifndef SRC_ASSET_MATERIAL_H__
#define SRC_ASSET_MATERIAL_H__

#include <vector>

class Texture;

class Material {
 public:
    explicit Material(const unsigned int &ID);
    virtual ~Material();

    unsigned int GetID() const { return m_ID; }

    const std::vector<unsigned int> &GetTextureIDs() const { return m_TextureIDs; }
    void AddTextureID(const unsigned int &texID);

 private:
    unsigned int m_ID;
    std::vector<unsigned int> m_TextureIDs;
};

#endif  // SRC_ASSET_MATERIAL_H__


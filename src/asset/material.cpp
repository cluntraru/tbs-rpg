#include <asset/material.h>
#include <asset/texture.h>

Material::Material(const unsigned int &ID) {
    m_ID = ID;
}

Material::~Material() {
}

void Material::AddTextureID(const unsigned int &textureID) {
    m_TextureIDs.push_back(textureID);
}


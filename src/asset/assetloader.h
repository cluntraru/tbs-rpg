#ifndef SRC_ASSET_ASSETLOADER_H__
#define SRC_ASSET_ASSETLOADER_H__

#include <string>

class Model;
class Texture;

class AssetLoader {
 public:
    AssetLoader();
    virtual ~AssetLoader();

    virtual Model *LoadModel(const std::string &path) = 0;
    virtual Texture *LoadTexture(const std::string &path) = 0;
};

#endif  // SRC_ASSET_ASSETLOADER_H__

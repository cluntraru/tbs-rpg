#ifndef SRC_ASSET_MESH_H__
#define SRC_ASSET_MESH_H__

#include <vector>

class Vertex;

class Mesh {
 public:
    explicit Mesh(const unsigned int &ID);
    virtual ~Mesh();

    void AddVertex(const Vertex &vertex);
    void AddDrawIdx(const unsigned int &idx);

    unsigned int GetID() const { return m_ID; }

    unsigned int GetMaterialID() const { return m_MaterialID; }
    void SetMaterialID(const unsigned int &idx) { m_MaterialID = idx; }

    inline const std::vector<Vertex> &GetVertices() const { return m_Vertices; }

    inline const std::vector<unsigned int> GetDrawIndices() const { return m_DrawIndices; }

 private:
    unsigned int m_ID;

    unsigned int m_MaterialID;
    std::vector<Vertex> m_Vertices;
    std::vector<unsigned int> m_DrawIndices;
};

#endif  // SRC_ASSET_MESH_H__


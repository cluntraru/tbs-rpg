#ifndef SRC_ASSET_TEXTURE_H__
#define SRC_ASSET_TEXTURE_H__

#include <glad/glad.h>

#include <cstdint>
#include <string>

class Texture {
 public:
    explicit Texture(const unsigned int &ID);
    virtual ~Texture();

    int Load(const std::string &path);
    int Load();
    void Unload();

    inline unsigned int GetID() const { return m_ID; }
    inline GLenum GetInternalFormat() const  { return m_InternalFormat; }
    inline GLenum GetSizedInternalFormat() const { return m_SizedInternalFormat; }
    inline int GetWidth() const { return m_Width; }
    inline int GetHeight() const { return m_Height; }
    inline uint8_t *GetData() const { return m_Data; }

 private:
    unsigned int m_ID;
    bool m_IsLoaded = false;

    std::string m_Path;
    uint8_t *m_Data = nullptr;
    GLenum m_InternalFormat;
    GLenum m_SizedInternalFormat;
    int m_Width;
    int m_Height;
};

#endif  // SRC_ASSET_TEXTURE_H__


#include <asset/mesh.h>
#include <render/vertex.h>

Mesh::Mesh(const unsigned int &ID) {
    m_ID = ID;
}

Mesh::~Mesh() {
}

void Mesh::AddVertex(const Vertex &vertex) {
    m_Vertices.push_back(vertex);
}

void Mesh::AddDrawIdx(const unsigned int &idx) {
    m_DrawIndices.push_back(idx);
}

#include <asset/texture.h>

#include <glad/glad.h>

#include <stb_image.h>

Texture::Texture(const unsigned int &ID) {
    m_ID = ID;
}

Texture::~Texture() {
    Unload();
}

int Texture::Load(const std::string &path) {
    if (m_IsLoaded == true) {
        printf("Texture already loaded, doing nothing\n");
        return 0;
    }

    m_Path = path;

    int colorComponentCnt;
    m_Data = stbi_load(path.c_str(), &m_Width, &m_Height, &colorComponentCnt, 0);
    if (m_Data == nullptr) {
        printf("stbi failed to load texture data\n");
        return -1;
    }

    switch (colorComponentCnt) {
        case 1:
            m_InternalFormat = GL_RED;
            // Untested
            m_SizedInternalFormat = GL_R8;
            break;
        case 3:
            m_InternalFormat = GL_RGB;
            m_SizedInternalFormat = GL_RGB8;
            break;
        case 4:
            m_InternalFormat = GL_RGBA;
            m_SizedInternalFormat = GL_RGBA8;
            break;
        default:
            printf("Texture internal format not supported, load aborted\n");
            return -1;
    }

    m_IsLoaded = true;
    return 0;
}

int Texture::Load() {
    return Load(m_Path);
}

void Texture::Unload() {
    if (m_IsLoaded == false) {
        return;
    }

    if (m_Data != nullptr) {
        stbi_image_free(m_Data);
        m_Data = nullptr;
    }

    m_Width = -1;
    m_Height = -1;
    m_InternalFormat = -1;
    m_SizedInternalFormat = -1;

    m_IsLoaded = false;
}


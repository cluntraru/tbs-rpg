#ifndef SRC_ASSET_ASSIMPASSETLOADER_H__
#define SRC_ASSET_ASSIMPASSETLOADER_H__

#include <asset/assetloader.h>

#include <map>
#include <string>
#include <utility>

class aiMaterial;
class aiMesh;
class aiNode;
class aiScene;

class Material;
class Mesh;
class RenderingComponent;
class Texture;
class World;

class AssimpAssetLoader: public AssetLoader {
 public:
    explicit AssimpAssetLoader(RenderingComponent *renderingComponent);
    virtual ~AssimpAssetLoader();

    Model *LoadModel(const std::string &path) override;
    Texture *LoadTexture(const std::string &path) override;

 private:
    int LoadMeshes(Model *model, const std::string &modelDir, const aiScene *aimpScene,
                   const aiNode *aimpNode);

    int PopulateMesh(Model *const &model, Mesh *const &mesh, const std::string &modelDir,
                     const aiScene *aimpScene, const aiMesh *aimpMesh);

    Material *LoadMaterial(const std::string &modelDir, const aiScene *aimpScene,
                           const unsigned int &modelID, const unsigned int &aimpMaterialIdx);

    // Gets internal ID of model from file path
    std::map<std::string, unsigned int> m_LoadedModelID;
    // Gets internal ID of texture from file path
    std::map<std::string, unsigned int> m_LoadedTextureID;
    // Gets internal ID of material from Assimp ID and model ID
    std::map<std::pair<unsigned int, unsigned int>, unsigned int> m_LoadedMaterialID;

    RenderingComponent *m_RenderingComponent;
};

#endif  // SRC_ASSET_ASSIMPASSETLOADER_H__


#include <climits>
#include <cstdio>

#include <asset/mesh.h>
#include <asset/model.h>

Model::Model(const unsigned int &ID) {
    m_ID = ID;
}

Model::~Model() {
    DeregisterMeshes();
}

Mesh *Model::RegisterMesh() {
    if (m_LastRegisteredMeshID == UINT_MAX) {
        printf("No more indices available for new meshes\n");
        return nullptr;
    }

    Mesh *mesh = new Mesh(++m_LastRegisteredMeshID);
    m_Meshes.push_back(mesh);
    return mesh;
}

void Model::DeregisterMeshes() {
    for (Mesh *const &mesh: m_Meshes) {
        delete mesh;
    }

    m_Meshes.clear();
}

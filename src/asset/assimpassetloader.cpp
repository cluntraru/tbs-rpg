#include <climits>

#include <glad/glad.h>

#include <stb_image.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <asset/assimpassetloader.h>
#include <asset/material.h>
#include <asset/mesh.h>
#include <asset/model.h>
#include <asset/texture.h>

#include <render/renderingcomponent.h>
#include <render/vertex.h>

AssimpAssetLoader::AssimpAssetLoader(RenderingComponent *renderingComponent) {
    m_RenderingComponent = renderingComponent;
}

AssimpAssetLoader::~AssimpAssetLoader() {
}

Model *AssimpAssetLoader::LoadModel(const std::string &path) {
    if (m_LoadedModelID.count(path) != 0) {
        unsigned int modelID = m_LoadedModelID[path];
        return m_RenderingComponent->GetModel(modelID);
    }

    aiScene *aimpScene = nullptr;
    Assimp::Importer importer;
    aimpScene = const_cast<aiScene *>(importer.ReadFile(path, aiProcess_Triangulate |
                                                              aiProcess_FlipUVs));

    if (!aimpScene || aimpScene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !aimpScene->mRootNode) {
        printf("Assimp failed to load scene from: %s\n", path.c_str());
        return nullptr;
    }

    const int directoryLen = path.find_last_of('/') + 1;
    const std::string &modelDirectory = path.substr(0, directoryLen);

    Model *model = m_RenderingComponent->RegisterModel();
    if (model == nullptr) {
        printf("No model was registered\n");
        return nullptr;
    }

    int loadMeshesError = LoadMeshes(model, modelDirectory, aimpScene, aimpScene->mRootNode);
    if (loadMeshesError) {
        printf("Failed to load meshes for model %s\n", path.c_str());
        return nullptr;
    }

    // TODO(cluntraru): encapsulate
    m_LoadedModelID[path] = model->GetID();
    return model;
}

Material *AssimpAssetLoader::LoadMaterial(const std::string &modelDir, const aiScene *aimpScene,
                                          const unsigned int &modelID,
                                          const unsigned int &aimpMaterialIdx) {
    if (m_LoadedMaterialID.count({modelID, aimpMaterialIdx}) != 0) {
        unsigned int materialID =  m_LoadedMaterialID[{modelID, aimpMaterialIdx}];
        return m_RenderingComponent->GetMaterial(materialID);
    }

    // TODO(cluntraru): currently only loads diffuse textures for each material
    aiMaterial *aimpMaterial = aimpScene->mMaterials[aimpMaterialIdx];
    unsigned int diffuseTextureCnt = aimpMaterial->GetTextureCount(aiTextureType_DIFFUSE);

    Material *material = m_RenderingComponent->RegisterMaterial();

    if (material == nullptr) {
        printf("No material was registered\n");
        return nullptr;
    }

    // TODO(cluntraru): encapsulate
    m_LoadedMaterialID[{modelID, aimpMaterialIdx}] = material->GetID();

    for (unsigned int i = 0; i < diffuseTextureCnt; ++i) {
        aiString aimpTexturePath;
        aimpMaterial->GetTexture(aiTextureType_DIFFUSE, i, &aimpTexturePath);
        const std::string texturePath = modelDir + std::string(aimpTexturePath.C_Str());

        Texture *texture = LoadTexture(texturePath);
        if (texture == nullptr) {
            printf("No texture was loaded for path %s, skipping\n", texturePath.c_str());
            continue;
        }

        material->AddTextureID(texture->GetID());
    }

    return material;
}

Texture *AssimpAssetLoader::LoadTexture(const std::string &path) {
    if (m_LoadedTextureID.count(path) != 0) {
        unsigned int textureID = m_LoadedTextureID[path];
        return m_RenderingComponent->GetTexture(textureID);
    }

    Texture *texture = m_RenderingComponent->RegisterTexture();
    if (texture == nullptr) {
        printf("Texture %s failed to register\n", path.c_str());
        return nullptr;
    }

    // TODO(cluntraru): encapsulate
    m_LoadedTextureID[path] = texture->GetID();

    texture->Load(path);
    return texture;
}

int AssimpAssetLoader::LoadMeshes(Model *model, const std::string &modelDir,
                                  const aiScene *aimpScene, const aiNode *aimpNode) {
    if (model == nullptr) {
        return -1;
    }

    for (unsigned int i = 0; i < aimpNode->mNumMeshes; ++i) {
        Mesh *mesh = model->RegisterMesh();

        unsigned int aimpMeshIdx = aimpNode->mMeshes[i];
        aiMesh *aimpMesh = aimpScene->mMeshes[aimpMeshIdx];
        int populateMeshError = PopulateMesh(model, mesh, modelDir, aimpScene, aimpMesh);
        if (populateMeshError) {
            return populateMeshError;
        }
    }

    for (unsigned int i = 0; i < aimpNode->mNumChildren; ++i) {
        const aiNode *aimpChild = aimpNode->mChildren[i];
        if (aimpChild == nullptr) {
            printf("Invalid child on Assimp node\n");
            return -1;
        }

        int loadMeshesError = LoadMeshes(model, modelDir, aimpScene, aimpChild);
        if (loadMeshesError) {
            return loadMeshesError;
        }
    }

    return 0;
}

int AssimpAssetLoader::PopulateMesh(Model *const &model, Mesh *const &mesh,
                                    const std::string &modelDir, const aiScene *aimpScene,
                                    const aiMesh *aimpMesh) {
    for (unsigned int i = 0; i < aimpMesh->mNumVertices; ++i) {
        const aiVector3D &aimpVertex = aimpMesh->mVertices[i];
        const glm::vec3 &position = glm::vec3(aimpVertex.x, aimpVertex.y, aimpVertex.z);

        const aiVector3D &aimpNormal = aimpMesh->mNormals[i];
        const glm::vec3 &normal = glm::vec3(aimpNormal.x, aimpNormal.y, aimpNormal.z);

        glm::vec2 texCoords = glm::vec2(0.f, 0.f);
        if (aimpMesh->mTextureCoords[0]) {
            const aiVector3D &aimpTexCoords = aimpMesh->mTextureCoords[0][i];
            texCoords = glm::vec2(aimpTexCoords.x, aimpTexCoords.y);
        }

        Vertex vertex(position, normal, texCoords);
        mesh->AddVertex(vertex);
    }

    for (unsigned int i = 0; i < aimpMesh->mNumFaces; ++i) {
        aiFace &aimpFace = aimpMesh->mFaces[i];

        for (unsigned int j = 0; j < aimpFace.mNumIndices; ++j) {
            mesh->AddDrawIdx(aimpFace.mIndices[j]);
        }
    }

    unsigned int aimpMaterialIdx = aimpMesh->mMaterialIndex;
    Material *material = LoadMaterial(modelDir, aimpScene, model->GetID(), aimpMaterialIdx);
    if (material == nullptr) {
        printf("No material was loaded\n");
        return -1;
    }

    mesh->SetMaterialID(material->GetID());
    return 0;
}


#ifndef SRC_TRANSFORM_H__
#define SRC_TRANSFORM_H__

#include <glm/glm.hpp>
#include <glm/ext/quaternion_float.hpp>

#include <helper/mathhelper.h>

// Everything in Transform is in local space, relative to the coord space of the parent entity
// Root entity is considered to be in World space
class Transform {
 public:
    static const glm::vec4 &s_AxisY;
    static const Transform &s_Identity;

    Transform();
    explicit Transform(const glm::vec4 &translation);
    Transform(const glm::vec4 &translation, const glm::vec4 &direction);
    Transform(const glm::vec4 &translation, const glm::quat &rotation, const float &scale = 1.f);
    ~Transform();

    void SetTransform(const glm::vec4 &translation, const glm::vec4 &direction);
    void SetTransform(const glm::vec4 &translation, const glm::quat &rotation,
                      const float &scale = 1.f);

    inline glm::vec4 GetDirection() const { return m_Direction; }
    void SetDirection(const glm::vec4 &direction);

    inline const glm::vec4 &GetTranslation() const { return m_Translation; }
    void SetTranslation(const glm::vec4 &translation);

    inline glm::quat GetRotation() const { return m_Rotation; }
    void SetRotation(const glm::quat &rotation);

    inline glm::vec4 GetUp() const { return m_Up; }
    inline glm::vec4 GetRight() const { return m_Right; }

    inline float GetScale() const { return m_Scale; }
    inline void SetScale(const float &scale) { m_Scale = scale; }

    void Translate(const glm::vec3 &translation);
    void Rotate(const glm::quat &rotation);

    inline float GetOXAngle() const { return m_OXAngle; }
    inline float GetOYAngle() const { return m_OYAngle; }
    inline float GetOZAngle() const { return m_OZAngle; }

    void ApplyPitch(const float &radians);
    void SetPitch(const float &radians);

    void ApplyYaw(const float &radians);
    void SetYaw(const float &radians);

    void ApplyRoll(const float &radians);
    void SetRoll(const float &radians);

    glm::mat4 CalcMatrix() const;

 private:
    static const glm::vec4 &s_Origin;
    static const glm::vec4 &s_BaseDirection;

    void UpdateData();
    void UpdateDataNoAngles();

    void SetDirectionUnsafe(const glm::vec4 &translation);
    void SetRotationUnsafe(const glm::quat &rotation);
    void SetTranslationUnsafe(const glm::vec4 &translation);

    // Always derived from rotation and translation
    glm::vec4 m_Direction;
    glm::vec4 m_Up;
    glm::vec4 m_Right;

    float m_OXAngle = 0.f;
    float m_OYAngle = 0.f;
    float m_OZAngle = 0.f;

    // All other values are derived from these
    glm::quat m_Rotation = MathHelper::g_IdentityRotate;
    glm::vec4 m_Translation = MathHelper::g_IdentityTranslate;
    float m_Scale = 1.f;
};

#endif  // SRC_TRANSFORM_H__

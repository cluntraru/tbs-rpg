#include <entity.h>

#include <asset/assetloader.h>
#include <world.h>

Entity::Entity(World *const &world) {
    m_World = world;
}

Entity::Entity(World *const &world, const Transform &transform) {
    m_World = world;
    m_Transform = transform;
}

Entity::~Entity() {
}

void Entity::LoadModel(const std::string &modelPath) {
    AssetLoader *assetLoader = GetWorld()->GetAssetLoader();
    m_Model = assetLoader->LoadModel(modelPath);
}

void Entity::AddChild(Entity *entity) {
    m_Children.push_back(entity);
    entity->m_Parent = this;
}

void Entity::RemoveChild(Entity *entity) {
    m_Children.remove(entity);
    entity->m_Parent = nullptr;
}

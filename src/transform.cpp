#include <transform.h>

#include <glm/ext/quaternion_float.hpp>

#include <helper/mathhelper.h>

const glm::vec4 &Transform::s_Origin = glm::vec4(0.f, 0.f, 0.f, 1.f);
const glm::vec4 &Transform::s_BaseDirection = glm::vec4(0.f, 0.f, -1.f, 0.f);
const glm::vec4 &Transform::s_AxisY = glm::vec4(0.f, 1.f, 0.f, 0.f);
const Transform &Transform::s_Identity = Transform(MathHelper::g_IdentityTranslate,
                                                   MathHelper::g_IdentityRotate);

Transform::Transform() {
    SetTransform(s_Origin, s_BaseDirection);
}

Transform::Transform(const glm::vec4 &position) {
    SetTransform(position, s_BaseDirection);
}

Transform::Transform(const glm::vec4 &translation, const glm::vec4 &direction) {
    SetTransform(translation, direction);
}

Transform::Transform(const glm::vec4 &translation, const glm::quat &rotation, const float &scale) {
    SetTransform(translation, rotation, scale);
}

Transform::~Transform() {
}

void Transform::SetTransform(const glm::vec4 &translation, const glm::vec4 &direction) {
    SetTranslationUnsafe(translation);
    SetDirectionUnsafe(direction);
    UpdateData();
}

void Transform::SetTransform(const glm::vec4 &translation, const glm::quat &rotation,
                             const float &scale) {
    SetTranslationUnsafe(translation);
    SetRotationUnsafe(rotation);
    SetScale(scale);
    UpdateData();
}

void Transform::SetDirection(const glm::vec4 &direction) {
    SetDirectionUnsafe(direction);
    UpdateData();
}

void Transform::SetRotation(const glm::quat &rotation) {
    SetRotationUnsafe(rotation);
    UpdateData();
}

void Transform::SetTranslation(const glm::vec4 &translation) {
    SetTranslationUnsafe(translation);
    UpdateData();
}

void Transform::Translate(const glm::vec3 &translation) {
    SetTranslationUnsafe(MathHelper::VecSum(m_Translation, glm::vec4(translation, 0.f)));
    UpdateDataNoAngles();
}

void Transform::Rotate(const glm::quat &rotation) {
    m_Rotation = MathHelper::MultiplyQuat(rotation, m_Rotation);
    UpdateData();
}

glm::mat4 Transform::CalcMatrix() const {
    const glm::mat4 &scaleMat = glm::mat4(m_Scale, 0, 0, 0,
                                          0, m_Scale, 0, 0,
                                          0, 0, m_Scale, 0,
                                          0, 0, 0, 1);

    const glm::mat4 &translateMat = glm::mat4(1, 0, 0, 0,
                                              0, 1, 0, 0,
                                              0, 0, 1, 0,
                                              m_Translation.x, m_Translation.y, m_Translation.z, 1);

    const glm::mat4 &rotateMat = MathHelper::QuatToMat(m_Rotation);

    glm::mat4 res = translateMat * rotateMat * scaleMat;
    return translateMat * rotateMat * scaleMat;
}

void Transform::ApplyPitch(const float &radians) {
    SetPitch(MathHelper::RadianSum(m_OXAngle, radians));
}

void Transform::SetPitch(const float &radians) {
    m_OXAngle = radians;
    m_Rotation = MathHelper::XYZToQuat(glm::vec3(m_OXAngle, m_OYAngle, m_OZAngle));
    UpdateDataNoAngles();
}

void Transform::ApplyYaw(const float &radians) {
    SetYaw(MathHelper::RadianSum(m_OYAngle, radians));
}

void Transform::SetYaw(const float &radians) {
    m_OYAngle = radians;
    m_Rotation = MathHelper::XYZToQuat(glm::vec3(m_OXAngle, m_OYAngle, m_OZAngle));
    UpdateDataNoAngles();
}

void Transform::ApplyRoll(const float &radians) {
    SetRoll(MathHelper::RadianSum(m_OZAngle, radians));
}

void Transform::SetRoll(const float &radians) {
    m_OZAngle = radians;
    m_Rotation = MathHelper::XYZToQuat(glm::vec3(m_OXAngle, m_OYAngle, m_OZAngle));
    UpdateDataNoAngles();
}

void Transform::SetDirectionUnsafe(const glm::vec4 &direction) {
    SetRotationUnsafe(MathHelper::DirectionToRotation(direction, s_BaseDirection));
}

void Transform::SetRotationUnsafe(const glm::quat &rotation) {
    m_Rotation = rotation;
}

void Transform::SetTranslationUnsafe(const glm::vec4 &translation) {
    m_Translation = translation;
}

void Transform::UpdateDataNoAngles() {
    m_Direction = MathHelper::RotatedDirection(s_BaseDirection, m_Rotation);

    m_Up = MathHelper::UpFromQuat(m_Rotation);
    m_Right = MathHelper::RightFromQuat(m_Rotation);
}

void Transform::UpdateData() {
    UpdateDataNoAngles();

    const glm::vec3 &axisAngles = MathHelper::QuatToXYZ(m_Rotation);
    m_OXAngle = axisAngles.x;
    m_OYAngle = axisAngles.y;
    m_OZAngle = axisAngles.z;
}


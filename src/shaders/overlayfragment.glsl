#version 450 core

out vec4 fragColor;

layout (location = 3) uniform int uSelectionType;

void main() {
    if (uSelectionType == 0) {
        fragColor = vec4(0.f, 1.f, 0.f, 0.4f);
    } else if (uSelectionType == 1) {
        fragColor = vec4(1.f, 0.f, 0.f, 0.4f);
    } else if (uSelectionType == 2) {
        fragColor = vec4(1.f, 0.f, 0.5f, 0.4f);
    }
}

#version 450 core
in vec2 textureCoord;
in vec3 viewNormal;
in vec3 viewLight;
in vec3 viewDir;

out vec4 fragColor;

layout (binding = 0) uniform sampler2D textureDiffuse;

layout (location = 4) uniform vec3 uAmbientAlbedo = vec3(0.5f);
layout (location = 5) uniform vec3 uDiffuseAlbedo = vec3(0.5f);
layout (location = 6) uniform vec3 uSpecularAlbedo = vec3(0.5f);
layout (location = 7) uniform float uSpecularPower = 16.f;
layout (location = 8) uniform float uAlpha = 1.f;

void main() {
    vec3 nNormal = normalize(viewNormal);
    vec3 nLight = normalize(viewLight);
    vec3 nViewDir = normalize(viewDir);

    vec3 reflection = reflect(-nLight, nNormal);

    vec3 diffuse = max(dot(nNormal, nLight), 0.f) * uDiffuseAlbedo;

    bool shouldSpecularShow = dot(nNormal, nLight) > 0;
    vec3 specular = pow(max(dot(reflection, nViewDir), 0.f), uSpecularPower) * uSpecularAlbedo * float(shouldSpecularShow);

    fragColor = vec4(uAmbientAlbedo + diffuse, uAlpha) * texture(textureDiffuse, textureCoord) + vec4(specular, 0.f);
}

#version 450 core
layout (location = 0) in vec4 inVertexCoord;

layout (location = 0) uniform mat4 uModel;
layout (location = 1) uniform mat4 uView;
layout (location = 2) uniform mat4 uProjection;

void main() {
    mat4 modelView = uView * uModel;
    vec4 viewCoord = modelView * inVertexCoord;
    gl_Position = uProjection * viewCoord;
}

#version 450 core
layout (location = 0) in vec4 inVertexCoord;
layout (location = 1) in vec2 inTextureCoord;
layout (location = 2) in vec4 inNormal;

out vec2 textureCoord;
out vec3 viewNormal;
out vec3 viewLight;
out vec3 viewDir;

layout (location = 0) uniform mat4 uModel;
layout (location = 1) uniform mat4 uView;
layout (location = 2) uniform mat4 uProjection;
layout (location = 3) uniform vec4 uLightCoord = vec4(0.f, 1.f, 0.f, 0.f);

void main() {
    textureCoord = inTextureCoord;
    mat4 modelView = uView * uModel;
    vec4 viewCoord = modelView * inVertexCoord;

    // Lighting
    viewNormal = mat3(transpose(inverse(modelView))) * normalize(inNormal.xyz);
    viewLight = mat3(uView) * normalize(uLightCoord.xyz) - normalize(viewCoord.xyz);
    viewDir = normalize(-viewCoord.xyz);

    gl_Position = uProjection * viewCoord;
}

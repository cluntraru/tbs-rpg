#ifndef SRC_HELPER_MATHHELPER_H__
#define SRC_HELPER_MATHHELPER_H__

#include <glm/ext/quaternion_float.hpp>

namespace MathHelper {
    enum Axis {
        OX,
        OY,
        OZ
    };

    enum AxisPlane {
        XOY,
        XOZ,
        YOZ
    };

    extern const float &g_Epsilon;
    extern const float &g_Pi;

    extern const glm::vec4 &g_OX;
    extern const glm::vec4 &g_OY;
    extern const glm::vec4 &g_OZ;

    extern const glm::quat &g_IdentityRotate;
    extern const glm::vec4 &g_IdentityTranslate;

    inline bool eq(const float &a, const float &b) {
        return a - b < g_Epsilon && a - b > -g_Epsilon;
    }

    inline bool eq(const glm::vec4 &a, const glm::vec4 &b) {
        const glm::vec4 diff = a - b;
        return eq(diff.x, 0.f) && eq(diff.y, 0.f) && eq(diff.z, 0.f) && eq(diff.z, 0.f);
    }

    inline bool gt(const float &a, const float &b) {
        return a - b > g_Epsilon;
    }

    inline bool lt(const float &a, const float &b) {
        return a - b < -g_Epsilon;
    }

    inline bool geq(const float &a, const float &b) {
        return a - b > -g_Epsilon;
    }

    inline bool leq(const float &a, const float &b) {
        return a - b < g_Epsilon;
    }

    glm::vec4 VecSum(const glm::vec4 &u, const glm::vec4 &v);
    float RadianSum(const float &a, const float &b);

    glm::vec4 Normal(const glm::vec4 &u, const glm::vec4 &v);

    float Angle(const glm::vec4 &u, const glm::vec4 &v);
    // True if v is counterclockwise of u from the PoV of O
    bool IsCounterClockwiseOf(const AxisPlane &plane, const glm::vec4 &u, const glm::vec4 &v);

    glm::vec4 Project(const Axis &axis, const glm::vec4 &v);
    glm::vec4 Project(const AxisPlane &plane, const glm::vec4 &v);

    glm::vec4 FrontFromQuat(const glm::quat &quat);
    glm::vec4 UpFromQuat(const glm::quat &quat);
    glm::vec4 RightFromQuat(const glm::quat &quat);

    glm::quat AxisAngleToQuat(const glm::vec4 &axis, const float &angle);

    // Default Dir is considered vec(-OZ)
    // Default Up is considered vec(OY)
    float AngleOnAxis(const Axis &axis, const glm::vec4 &front, const glm::vec4 &up);
    glm::vec3 QuatToXYZ(const glm::quat &quat);
    glm::quat XYZToQuat(const glm::vec3 &angles);

    glm::mat4 QuatToMat(const glm::quat &quat);

    glm::mat4 ChainTranslation(const glm::mat4 &baseTranslation, const glm::vec3 &newTranslation);
    glm::quat MultiplyQuat(const glm::quat &baseRotation, const glm::quat &newRotation);

    glm::vec4 TranslatedPosition(const glm::vec4 &position, const glm::mat4 &translation);
    glm::vec4 RotatedDirection(const glm::vec4 &direction, const glm::quat &rotation);

    glm::quat VecToQuat(const glm::vec4 &vec);
    glm::vec4 QuatToVec(const glm::quat &quat);

    glm::mat4 AbsPositionToTranslation(const glm::vec4 &position, const glm::vec4 &basePosition);
    glm::quat DirectionToRotation(const glm::vec4 &direction, const glm::vec4 &baseDirection);

    glm::vec3 Lerp(const glm::vec3 &src, const glm::vec3 &dst, const float &ratio);
}  // namespace MathHelper

#endif  // SRC_HELPER_MATHHELPER_H__

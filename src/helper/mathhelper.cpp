#include <helper/mathhelper.h>

#include <algorithm>

#include <glm/ext/quaternion_float.hpp>
#include <glm/gtc/quaternion.hpp>
#include <cstdio>

const float &MathHelper::g_Epsilon = 1e-6;
const float &MathHelper::g_Pi = 3.1415926535f;

const glm::vec4 &MathHelper::g_OX = glm::vec4(1.f, 0.f, 0.f, 0.f);
const glm::vec4 &MathHelper::g_OY = glm::vec4(0.f, 1.f, 0.f, 0.f);
const glm::vec4 &MathHelper::g_OZ = glm::vec4(0.f, 0.f, 1.f, 0.f);

const glm::quat &MathHelper::g_IdentityRotate = glm::quat(1.f, 0.f, 0.f, 0.f);
const glm::vec4 &MathHelper::g_IdentityTranslate = glm::vec4(0.f);

glm::vec4 MathHelper::VecSum(const glm::vec4 &u, const glm::vec4 &v) {
    glm::vec4 sum = u + v;
    sum.w = std::max(u.w, v.w);
    return sum;
}

float MathHelper::RadianSum(const float &a, const float &b) {
    float sum = a + b;
    if (gt(sum, g_Pi)) {
        sum -= 2 * g_Pi;
    } else if (lt(sum, -g_Pi)) {
        sum += 2 * g_Pi;
    }

    return sum;
}

glm::vec4 MathHelper::Normal(const glm::vec4 &u, const glm::vec4 &v) {
    const glm::vec3 &u3(u);
    const glm::vec3 &v3(v);

    const glm::vec3 &normal = glm::normalize(glm::cross(u3, v3));
    return glm::vec4(normal, 0.f);
}

float MathHelper::Angle(const glm::vec4 &u, const glm::vec4 &v) {
    const glm::vec3 &u3(u);
    const glm::vec3 &v3(v);

    const float &magProduct = glm::length(u3) * glm::length(v3);
    const float &dot = glm::dot(u3, v3);
    return glm::acos(dot / magProduct);
}

bool MathHelper::IsCounterClockwiseOf(const AxisPlane &plane, const glm::vec4 &u,
                                      const glm::vec4 &v) {
    switch (plane) {
     case XOY:
        return gt(u.x * v.y - u.y * v.x, 0.f);
     case XOZ:
        return gt((-u.x) * v.z - u.z * (-v.x), 0.f);
     case YOZ:
        return gt(-u.z * v.y - u.y * (-v.z), 0.f);
     default:
        printf("Unsupported plane, returning\n");
        return false;
    }
}

glm::vec4 MathHelper::Project(const Axis &axis, const glm::vec4 &v) {
    switch (axis) {
     case OX:
        return glm::vec4(v.x, 0.f, 0.f, v.w);
     case OY:
        return glm::vec4(0.f, v.y, 0.f, v.w);
     case OZ:
        return glm::vec4(0.f, 0.f, v.z, v.w);
     default:
        printf("Unsupported axis, returning");
        return glm::vec4(0.f, 0.f, 0.f, v.w);
    }
}

glm::vec4 MathHelper::Project(const AxisPlane &plane, const glm::vec4 &v) {
    const glm::vec4 &vOX = Project(OX, v);
    const glm::vec4 &vOY = Project(OY, v);
    const glm::vec4 &vOZ = Project(OZ, v);
    switch (plane) {
     case XOY:
        return VecSum(vOX, vOY);
     case XOZ:
        return VecSum(vOX, vOZ);
     case YOZ:
        return VecSum(vOY, vOZ);
     default:
        printf("Unsupported plane, returning\n");
        return glm::vec4(0.f, 0.f, 0.f, v.w);
    }
}

glm::vec4 MathHelper::FrontFromQuat(const glm::quat &quat) {
    // vec(-OZ) is default front
    return RotatedDirection(-g_OZ, quat);
}

glm::vec4 MathHelper::UpFromQuat(const glm::quat &quat) {
    // vec(OY) is default up
    return RotatedDirection(g_OY, quat);
}

glm::vec4 MathHelper::RightFromQuat(const glm::quat &quat) {
    return RotatedDirection(g_OX, quat);
}

glm::quat MathHelper::AxisAngleToQuat(const glm::vec4 &axis, const float &angle) {
    const float &halfAngle = angle / 2.f;
    const float &sinHalfAngle = glm::sin(halfAngle);
    const float &cosHalfAngle = glm::cos(halfAngle);
    return glm::quat(cosHalfAngle, sinHalfAngle * axis.x,
                                   sinHalfAngle * axis.y,
                                   sinHalfAngle * axis.z);
}

float MathHelper::AngleOnAxis(const Axis &axis, const glm::vec4 &front, const glm::vec4 &up) {
    switch (axis) {
     case OX: {
        const glm::vec4 &projected = Project(YOZ, front);
        if (eq(projected, glm::vec4(0.f))) {
            return 0.f;
        }

        float angle = IsCounterClockwiseOf(YOZ, -g_OZ, projected) ? Angle(-g_OZ, projected) :
                                                                    -Angle(-g_OZ, projected);
        return angle;
     }
     case OY: {
        const glm::vec4 &projected = Project(XOZ, front);
        if (eq(projected, glm::vec4(0.f))) {
            return 0.f;
        }

        float angle = IsCounterClockwiseOf(XOZ, -g_OZ, projected) ? Angle(-g_OZ, projected) :
                                                                    -Angle(-g_OZ, projected);
        return angle;
     }
     case OZ: {
        const glm::vec4 &projected = Project(XOY, up);
        if (eq(projected, glm::vec4(0.f))) {
            return 0.f;
        }

        float angle = IsCounterClockwiseOf(XOY, g_OY, projected) ? Angle(g_OY, projected) :
                                                                   -Angle(g_OY, projected);
        return angle;
     }
     default:
         return 0.f;
    }
}

glm::vec3 MathHelper::QuatToXYZ(const glm::quat &quat) {
    const glm::vec4 &newUp = UpFromQuat(quat);
    const glm::vec4 &newFront = FrontFromQuat(quat);

    const float &angleOX = AngleOnAxis(OX, newFront, newUp);
    const float &angleOY = AngleOnAxis(OY, newFront, newUp);
    const float &angleOZ = AngleOnAxis(OZ, newFront, newUp);

    return glm::vec3(angleOX, angleOY, angleOZ);
}

glm::quat MathHelper::XYZToQuat(const glm::vec3 &angles) {
    const glm::quat &rotOX = AxisAngleToQuat(g_OX, angles.x);
    const glm::quat &rotOY = AxisAngleToQuat(g_OY, angles.y);
    const glm::quat &rotOZ = AxisAngleToQuat(g_OZ, angles.z);
    return MultiplyQuat(rotOY, MultiplyQuat(rotOX, rotOZ));
}

glm::mat4 MathHelper::QuatToMat(const glm::quat &quat) {
    const float &x2 = quat.x * quat.x;
    const float &y2 = quat.y * quat.y;
    const float &z2 = quat.z * quat.z;
    const float &xy = quat.x * quat.y;
    const float &xz = quat.x * quat.z;
    const float &xw = quat.x * quat.w;
    const float &yz = quat.y * quat.z;
    const float &yw = quat.y * quat.w;
    const float &zw = quat.z * quat.w;

    glm::mat4 rotMat;

    rotMat[0][0] = 1 - 2 * y2 - 2 * z2;
    rotMat[0][1] = 2 * xy + 2 * zw;
    rotMat[0][2] = 2 * xz - 2 * yw;
    rotMat[0][3] = 0;

    rotMat[1][0] = 2 * xy - 2 * zw;
    rotMat[1][1] = 1 - 2 * x2 - 2 * z2;
    rotMat[1][2] = 2 * yz + 2 * xw;
    rotMat[1][3] = 0;

    rotMat[2][0] = 2 * xz + 2 * yw;
    rotMat[2][1] = 2 * yz - 2 * xw;
    rotMat[2][2] = 1 - 2 * x2 - 2 * y2;
    rotMat[2][3] = 0;

    rotMat[3][0] = 0;
    rotMat[3][1] = 0;
    rotMat[3][2] = 0;
    rotMat[3][3] = 1;

    return rotMat;
}

glm::mat4 MathHelper::ChainTranslation(const glm::mat4 &baseTranslation,
                                       const glm::vec3 &newTranslation) {
    return glm::translate(baseTranslation, newTranslation);
}

glm::quat MathHelper::MultiplyQuat(const glm::quat &quat1, const glm::quat &quat2) {
    return glm::cross(quat1, quat2);
}

glm::vec4 MathHelper::TranslatedPosition(const glm::vec4 &position, const glm::mat4 &translation) {
    return translation * position;
}

glm::vec4 MathHelper::RotatedDirection(const glm::vec4 &direction, const glm::quat &rotation) {
    glm::quat directionPureQuat = VecToQuat(direction);

    // Conjugate is inverse, all rotations are unit quaternions
    const glm::quat &inverseRotation = glm::conjugate(rotation);
    glm::quat rotatedPureQuat = MultiplyQuat(rotation,
                                             MultiplyQuat(directionPureQuat, inverseRotation));

    return QuatToVec(rotatedPureQuat);
}

glm::quat MathHelper::VecToQuat(const glm::vec4 &vec) {
    return glm::quat(0.f, vec.x, vec.y, vec.z);
}

glm::vec4 MathHelper::QuatToVec(const glm::quat &quat) {
    return glm::vec4(quat.x, quat.y, quat.z, 0.f);
}

glm::mat4 MathHelper::AbsPositionToTranslation(const glm::vec4 &position,
                                            const glm::vec4 &basePosition) {
    glm::vec3 translation = glm::vec3(position - basePosition);
    return glm::translate(glm::mat4(1.f), translation);
}

glm::quat MathHelper::DirectionToRotation(const glm::vec4 &direction,
                                          const glm::vec4 &baseDirection) {
    if (direction == baseDirection) {
        return MathHelper::g_IdentityRotate;
    }

    const glm::vec3 &axis = Normal(direction, baseDirection);
    const float &halfTheta = Angle(direction, baseDirection);

    const float &cosHalfTheta = glm::cos(halfTheta);
    const float &sinHalfTheta = glm::sin(halfTheta);

    return glm::quat(sinHalfTheta,
                     axis.x * cosHalfTheta,
                     axis.y * cosHalfTheta,
                     axis.z * cosHalfTheta);
}

glm::vec3 MathHelper::Lerp(const glm::vec3 &src, const glm::vec3 &dst, const float &ratio) {
    return src + (dst - src) * ratio;
}

#ifndef SRC_DATA_DATAPROVIDER_H__
#define SRC_DATA_DATAPROVIDER_H__

#include <string>
#include <vector>

#include <glm/glm.hpp>

class DataProvider {
 public:
    DataProvider();
    virtual ~DataProvider();

    virtual std::string GetBoardModelPath() const = 0;
    virtual std::string GetPieceModelPath(const std::string &name) const = 0;
    inline virtual std::string GetSlotOverlayModelPath() const {
        return "res/overlay/circular_overlay.obj";
    }

    virtual std::vector<glm::vec3> GetSlotCoords() const = 0;
    // vector of {piece name, slot id}
    virtual std::vector<std::pair<std::string, unsigned int>> GetPieceSpawnLocation() const = 0;
    virtual float GetPieceScale() const { return 1.f; }
    virtual glm::vec3 GetBoardEulerAngles() const = 0;

    inline virtual glm::vec3 GetSlotOverlayTranslate() const { return glm::vec3(0.f, 0.f, 0.05f); }
    inline virtual float GetSlotOverlayScale() const { return 6.5f; }
};

#endif  // SRC_DATA_DATAPROVIDER_H__


#include <data/jsondataprovider.h>

#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include <nlohmann/json.hpp>

JsonDataProvider::JsonDataProvider(const std::string &gameName) {
    std::string configPath;
    if (gameName == "chess") {
        configPath = "config/chess.json";
    } else if (gameName == "go") {
        configPath = "config/go.json";
    }

    m_Json = new nlohmann::json();

    std::ifstream jsonIn(configPath);
    jsonIn >> *m_Json;
    jsonIn.close();
}

JsonDataProvider::~JsonDataProvider() {
    delete m_Json;
}

std::string JsonDataProvider::GetBoardModelPath() const {
    nlohmann::json &json = *m_Json;
    return json["boardmodelpath"];
}

std::string JsonDataProvider::GetPieceModelPath(const std::string &name) const {
    nlohmann::json &json = *m_Json;
    return json["piecemodelpath"][name];
}

float JsonDataProvider::GetPieceScale() const {
    nlohmann::json &json = *m_Json;
    return json["piecescale"];
}

std::vector<glm::vec3> JsonDataProvider::GetSlotCoords() const {
    nlohmann::json &json = *m_Json;
    auto jsonSlotCoords = json["slotcoords"];

    std::vector<glm::vec3> slotCoords;
    for (auto jsonSlotCoord : jsonSlotCoords) {
        slotCoords.emplace_back(jsonSlotCoord[0], jsonSlotCoord[1], jsonSlotCoord[2]);
    }

    return slotCoords;
}

std::vector<std::pair<std::string, unsigned int>> JsonDataProvider::GetPieceSpawnLocation() const {
    std::vector<std::pair<std::string, unsigned int>> pieceSlots;

    nlohmann::json &json = *m_Json;
    auto jsonPieceSlots = json["piecespawnslot"];
    for (auto jsonPiece : jsonPieceSlots) {
        std::string pieceName = jsonPiece[0];
        for (unsigned long i = 1; i < jsonPiece.size(); ++i) {
            pieceSlots.emplace_back(std::pair<std::string, unsigned int>(pieceName, jsonPiece[i]));
        }
    }

    return pieceSlots;
}

glm::vec3 JsonDataProvider::GetBoardEulerAngles() const {
    nlohmann::json &json = *m_Json;
    auto jsonEulerAngles = json["boardeulerangles"];
    glm::vec3 eulerAngles = glm::vec3(jsonEulerAngles[0], jsonEulerAngles[1], jsonEulerAngles[2]);
    return eulerAngles;
}

#ifndef SRC_DATA_JSONDATAPROVIDER_H__
#define SRC_DATA_JSONDATAPROVIDER_H__

#include <data/dataprovider.h>

#include <nlohmann/json_fwd.hpp>

class JsonDataProvider: public DataProvider {
 public:
    explicit JsonDataProvider(const std::string &gameName);
    virtual ~JsonDataProvider();

    std::string GetBoardModelPath() const override;
    std::string GetPieceModelPath(const std::string &name) const override;
//    inline virtual std::string GetSlotOverlayModelPath() const override;

    std::vector<glm::vec3> GetSlotCoords() const override;

    // vector of {piece name, slot id}
    std::vector<std::pair<std::string, unsigned int>> GetPieceSpawnLocation() const override;
    float GetPieceScale() const override;
    glm::vec3 GetBoardEulerAngles() const override;

//    inline virtual glm::vec3 GetSlotOverlayTranslate() const override;
//    inline virtual float GetSlotOverlayScale() const override;

 private:
    nlohmann::json *m_Json;
};

#endif  // SRC_DATA_JSONDATAPROVIDER_H__
#ifndef SRC_DATA_MANUALDATAPROVIDER_H__
#define SRC_DATA_MANUALDATAPROVIDER_H__

#include <data/dataprovider.h>

class ManualDataProvider: public DataProvider {
 public:
    ManualDataProvider();
    virtual ~ManualDataProvider();

    std::string GetBoardModelPath() const override;
    std::string GetPieceModelPath(const std::string &name) const override;
    std::vector<glm::vec3> GetSlotCoords() const override;
    std::vector<std::pair<std::string, unsigned int>> GetPieceSpawnLocation() const override;
    float GetPieceScale() const override;
    glm::vec3 GetBoardEulerAngles() const override;
};

#endif  // SRC_DATA_MANUALDATAPROVIDER_H__

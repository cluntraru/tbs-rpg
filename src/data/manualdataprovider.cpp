#include <data/manualdataprovider.h>

#include <string>
#include <utility>
#include <vector>

#include <glm/glm.hpp>

#include <helper/mathhelper.h>

ManualDataProvider::ManualDataProvider() {
}

ManualDataProvider::~ManualDataProvider() {
}

std::string ManualDataProvider::GetBoardModelPath() const {
    return "res/chessboard/Board.3ds";
}

std::string ManualDataProvider::GetPieceModelPath(const std::string &name) const {
    if (name == "darkpawn") {
        return "res/chessdark/Pawn.3ds";
    } else if (name == "lightpawn") {
        return "res/chesslight/Pawn.3ds";
    } else if (name == "darkrook") {
        return "res/chessdark/Rook.3ds";
    } else if (name == "lightrook") {
        return "res/chesslight/Rook.3ds";
    } else if (name == "darkknight") {
        return "res/chessdark/Knight.3ds";
    } else if (name == "lightknight") {
        return "res/chesslight/Knight.3ds";
    } else if (name == "darkbishop") {
        return "res/chessdark/Bishop.3ds";
    } else if (name == "lightbishop") {
        return "res/chesslight/Bishop.3ds";
    } else if (name == "darkking") {
        return "res/chessdark/King.3ds";
    } else if (name == "lightking") {
        return "res/chesslight/King.3ds";
    } else if (name == "darkqueen") {
        return "res/chessdark/Queen.3ds";
    } else if (name == "lightqueen") {
        return "res/chesslight/Queen.3ds";
    }

    return "res/invalid";
}

std::vector<glm::vec3> ManualDataProvider::GetSlotCoords() const {
    std::vector<glm::vec3> slotCoords;
    const float &slotDist = 12.45f;
    const float &xTopLeft = -slotDist * 3.5f;
    const float &yTopLeft = slotDist * 3.5f;

    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            slotCoords.emplace_back(glm::vec3(xTopLeft + j * slotDist,
                                              yTopLeft - i * slotDist, 0.f));
        }
    }

    return slotCoords;
}

std::vector<std::pair<std::string, unsigned int>> ManualDataProvider::GetPieceSpawnLocation() const {
    std::vector<std::pair<std::string, unsigned int>> pieceSlots;
    for (int i = 0; i < 8; ++i) {
        pieceSlots.emplace_back(std::pair("darkpawn", 8 + i));
        pieceSlots.emplace_back(std::pair("lightpawn", 48 + i));
    }

    pieceSlots.emplace_back(std::pair("darkrook", 0));
    pieceSlots.emplace_back(std::pair("darkrook", 7));
    pieceSlots.emplace_back(std::pair("lightrook", 56));
    pieceSlots.emplace_back(std::pair("lightrook", 63));

    pieceSlots.emplace_back(std::pair("darkknight", 1));
    pieceSlots.emplace_back(std::pair("darkknight", 6));
    pieceSlots.emplace_back(std::pair("lightknight", 57));
    pieceSlots.emplace_back(std::pair("lightknight", 62));

    pieceSlots.emplace_back(std::pair("darkbishop", 2));
    pieceSlots.emplace_back(std::pair("darkbishop", 5));
    pieceSlots.emplace_back(std::pair("lightbishop", 58));
    pieceSlots.emplace_back(std::pair("lightbishop", 61));

    pieceSlots.emplace_back(std::pair("darkking", 3));
    pieceSlots.emplace_back(std::pair("lightking", 59));

    pieceSlots.emplace_back(std::pair("darkqueen", 4));
    pieceSlots.emplace_back(std::pair("lightqueen", 60));

    return pieceSlots;
}

float ManualDataProvider::GetPieceScale() const {
    return 0.8f;
}

glm::vec3 ManualDataProvider::GetBoardEulerAngles() const {
    return glm::vec3(-MathHelper::g_Pi / 2.f, 0.f, 0.f);
}

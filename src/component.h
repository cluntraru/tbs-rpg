#ifndef SRC_COMPONENT_H__
#define SRC_COMPONENT_H__

class World;

class Component {
 public:
    explicit Component(World *world);
    virtual ~Component();

    virtual int Init() = 0;
    virtual void Start() = 0;
    virtual void Update(float deltaTime) = 0;
    virtual void Stop() = 0;
    virtual void Destroy() = 0;

    virtual World *GetWorld() const { return m_World; }

 private:
    World *m_World;
};

#endif  // SRC_COMPONENT_H__

#ifndef SRC_DRAWABLE_H__
#define SRC_DRAWABLE_H__

#include <glm/glm.hpp>

class Model;

struct Drawable {
    Drawable(const glm::mat4 &transform, Model *const &model, const bool &isTransparent = false) {
        this->transform = transform;
        this->model = model;
        this->isTransparent = isTransparent;
    }

    glm::mat4 transform;
    Model *model;
    bool isTransparent;
};

#endif  // SRC_DRAWABLE_H__

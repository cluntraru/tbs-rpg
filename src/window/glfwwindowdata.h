#ifndef SRC_WINDOW_GLFWWINDOWDATA_H__
#define SRC_WINDOW_GLFWWINDOWDATA_H__

class GLFWwindow;

class GLFWWindowData {
 public:
    explicit GLFWWindowData(const int &width, const int &height);
    virtual ~GLFWWindowData();

    int Init();
    void Destroy();

    GLFWwindow *GetWindow() const { return m_GLFWWindow; }
    void SetWindow(GLFWwindow *glfwWindow) { m_GLFWWindow = glfwWindow; }
    void SetSize(const int &width, const int &height);

    void SetWidth(const int &width);
    int GetWidth() const { return m_Width; }

    void SetHeight(const int &height);
    int GetHeight() const { return m_Height; }

 private:
    GLFWwindow *m_GLFWWindow;
    int m_Width;
    int m_Height;
};

#endif  // SRC_WINDOW_GLFWWINDOWDATA_H__


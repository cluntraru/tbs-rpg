#include <window/glfwwindowdata.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

GLFWWindowData::GLFWWindowData(const int &width, const int &height) {
    m_Width = width;
    m_Height = height;
}

GLFWWindowData::~GLFWWindowData() {
}

int GLFWWindowData::Init() {
    m_GLFWWindow = glfwCreateWindow(m_Width, m_Height, "Turn Based Engine", NULL, NULL);
    glfwMakeContextCurrent(m_GLFWWindow);
    return 0;
}

void GLFWWindowData::Destroy() {
    if (m_GLFWWindow != nullptr) {
        glfwDestroyWindow(m_GLFWWindow);
    }
}

void GLFWWindowData::SetSize(const int &width, const int &height) {
    glViewport(0, 0, width, height);
    glfwSetWindowSize(m_GLFWWindow, width, height);
    m_Width = width;
    m_Height = height;
}

void GLFWWindowData::SetWidth(const int &width) {
    SetSize(width, m_Height);
}

void GLFWWindowData::SetHeight(const int &height) {
    SetSize(m_Width, height);
}

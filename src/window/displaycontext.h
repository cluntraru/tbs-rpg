#ifndef SRC_WINDOW_DISPLAYCONTEXT_H__
#define SRC_WINDOW_DISPLAYCONTEXT_H__

class GLFWWindowData;
class World;

class DisplayContext {
 public:
    static DisplayContext *GetInstance();

    virtual ~DisplayContext();

    int Init();
    void Destroy();

    inline GLFWWindowData *GetWindowData() const { return m_WindowData; }

    inline World *GetActiveWorld() const { return m_ActiveWorld; }
    inline void SetActiveWorld(World *world) { m_ActiveWorld = world; }

    float GetCurrentTime() const;

 private:
    static DisplayContext *m_Instance;
    explicit DisplayContext(const int &width = 1600, const int &height = 900);

    int InitGLFW();
    int InitGLAD();

    void DestroyGLFW();

    GLFWWindowData *m_WindowData;
    World *m_ActiveWorld;
};

#endif  // SRC_WINDOW_DISPLAYCONTEXT_H__


#include <window/displaycontext.h>

#include <cstdio>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <window/glfwwindowdata.h>
#include <world.h>

DisplayContext *DisplayContext::m_Instance = nullptr;

DisplayContext *DisplayContext::GetInstance() {
    if (m_Instance == nullptr) {
        m_Instance = new DisplayContext();
    }

    return m_Instance;
}

DisplayContext::DisplayContext(const int &width, const int &height) {
    m_WindowData = new GLFWWindowData(width, height);
}

DisplayContext::~DisplayContext() {
    delete m_WindowData;
}

int DisplayContext::Init() {
    int glfwError = InitGLFW();
    if (glfwError) {
        return -1;
    }

    int gladError = InitGLAD();
    if (gladError) {
        Destroy();
        return -1;
    }

    if (m_ActiveWorld == nullptr) {
        Destroy();
        return -1;
    }

    return 0;
}

void DisplayContext::Destroy() {
    DestroyGLFW();
}

int DisplayContext::InitGLFW() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    int windowDataError = m_WindowData->Init();
    if (windowDataError) {
        printf("Failed to init window, aborting\n");
        DestroyGLFW();
        return windowDataError;
    }

    return 0;
}

void DisplayContext::DestroyGLFW() {
    glfwSetWindowShouldClose(m_WindowData->GetWindow(), true);
    m_WindowData->Destroy();
    glfwTerminate();
}

int DisplayContext::InitGLAD() {
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        return -1;
    }

    return 0;
}

float DisplayContext::GetCurrentTime() const {
    return static_cast<float>(glfwGetTime());
}


# A turn-based game engine from scratch

A turn-based game engine providing an API to implement a great variety of board games.

#### Build
Make sure you are in / when calling cmake, otherwise YCM won't work
```bash
cmake .
make
```

#### Run
```bash
optirun ./tbs_rpg
```

#### Lint
Run Google cpplint as 
```bash
cpplint --recursive --linelength=100 --filter=-build/include_order,-runtime/references,-legal src
```

#### Engine Usage
To implement a game, inherit from GameplayComponent and implement virtual functions. You can also inherit from Player to simulate a very basic inventory system. Samples are available as GoGameplayComponent and ChessGameplayComponent - these are pretty messily implemented, since they are only meant as demos, but should convey usage pretty well.

